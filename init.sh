# -----------------------------------------------------------------------------
# Script     : init.sh.
# Description: Initializes environment variables and activates Python virtual
#              environment.
# -----------------------------------------------------------------------------

source venv/bin/activate
if [[ $? -ne 0 ]]; then
  printf "ERROR: Failed activating Python virtual environment.\n" >&2
  return 1
fi

PYTHONPATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd):${PYTHONPATH}"

export PYTHONPATH
