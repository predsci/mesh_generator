"""
Test out the phi mesh input that caused the interface to crash in the current mesh_generator package.
These numbers were created automatically based on a region and flux-rope selection in the interface.

I verified the crash this bug with pypi version 1.1.5 and the latest mesh_generator version in the bitcbucket.
- The bitbucket version has your updates/bugfixes beyond 1.1.5  (as of 10/7/2020, commit f60f5fd).
  - If I use the temporary fix (see below), this version (not 1.1.5) seems to produce a "correct" mesh.

NOTES:
        - This particular region was near the left (phi=0) boundary.
        - It looks like the 3rd segment is causing the crash. The automatic box has a negative p0 value.
        - This sort of region *should* be handled by insert_mesh_segment for a periodic case (split up, limits corrected).
        - Perhaps the problem occurs earlier, but if I look at the output json after the "adjusted mesh" step,
          there is a segment with backwards s0 and s1 (set verbose_debug=True).
        - BUT for some reason, if I just add 2*pi to p0 and p1 for segment 3 it works fine (temporary_fix=True).
        - So, I'm guessing the problem may be at the segment correction stage (input sanitization).
"""
import numpy
import pprint
import os

from mesh_generator.src.mesh import Mesh
from mesh_generator.src.mesh_segment import MeshSegment
from mesh_generator.bin.call_psi_mesh_tool import create_psi_mesh

# set the plot output folder
dir_name = os.getcwd()
temporary_fix = False
verbose_debug = False

# set the original four phi segments that crashed mesh_generator.
#                        seg1       seg2         seg3         seg4
p0_vec = numpy.array([0.186052, 0.217595, -0.13370181, 4.14235485])
p1_vec = numpy.array([0.415563, 0.387666,  0.73896281, 9.02927676])
ds_vec = numpy.array([0.004,    0.002187,        0.02,       0.04])

# apply a temporary fix (adding 2*pi to segment 3) to demo a working example / illustrate the problem
if temporary_fix:
    p0_vec[2] = p0_vec[2] + 2*numpy.pi
    p1_vec[2] = p1_vec[2] + 2*numpy.pi

# Set the default ratios (both the same)
MeshSegment.DEFAULT_BG_REGION_RATIO = 1.03
MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03

# Setup the initial Mesh.
mesh_type = 'p'
mesh = Mesh(lower_bnd=0.00, upper_bnd=2*numpy.pi, periodic=True)

# use this if you want to print the mesh json at each step (this helped me...)
indent = 2
strsep = "-"*72

# Loop over the segments add them to the mesh
for i, dummy in enumerate(p0_vec):
    print(f'Adding seg {i}: s0: {p0_vec[i]:8.5f}, s1: {p1_vec[i]:8.5f}, ds: {ds_vec[i]:8.5f}')
    mesh.insert_mesh_segment(MeshSegment(s0=float(p0_vec[i]), s1=float(p1_vec[i]), ds=float(ds_vec[i])))
    if verbose_debug:
        print(strsep)
        pprint.pprint(mesh.json_dict(), indent=indent)
        print('')

input_mesh = mesh.json_dict()

# print the initial mesh
if verbose_debug:
    print(f'\nMesh JSON: POST INSERTION\n{strsep}')
    pprint.pprint(mesh.json_dict())

# Build the adjusted mesh
adjusted_mesh = mesh.resolve_mesh_segments().json_dict()
if verbose_debug:
    print(f'\nMesh JSON: POST ADJUST\n{strsep}')
    pprint.pprint(adjusted_mesh)

# Build the legacy mesh
legacy_mesh = mesh.build_legacy_mesh().json_dict()
if verbose_debug:
    print(f'\nMesh JSON: POST LEGACY\n{strsep}')
    pprint.pprint(legacy_mesh)

# run the final mesh routine
create_psi_mesh(adjusted_mesh, legacy_mesh, mesh_type=mesh_type, dir_name=dir_name,
                output_file_name=f"tmp_mesh_{mesh_type}.dat",
                mesh_res_file_name=f"mesh_res_{mesh_type}.dat", save_plot=False, show_plot=True,
                save_plot_path=dir_name,
                plot_file_name=f"{mesh_type}_mesh_spacing.png", input_mesh=input_mesh)
