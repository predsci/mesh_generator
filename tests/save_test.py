"""
A tool to write the test results automatically in test_results.py and save output.dat file in the folder "output".
"""


def save_test(file_name: str, test_name: str, mesh_json: str):
    """ Save the adjusted mesh dictionary and legacy mesh dictionary in test_results.py
    later used for comparison in test/unittest.py"""
    f = open(file_name, 'a')  # append to the python file.
    mesh_json = mesh_json.replace("false", "False")
    mesh_json = mesh_json.replace("true", "True")
    mesh_json = mesh_json.replace("Infinity", "numpy.Infinity")
    f.write(str(test_name) + " = " + str(mesh_json) + '\n\n')
    f.close()


def duplicate_txt_file(output_file_name: str, duplicate_file_name: str):
    """ Save the output.dat file in the folder "output" later used for comparison in test/unittest.py"""
    with open(output_file_name) as f:
        with open(duplicate_file_name, "w") as f1:
            for line in f:
                f1.write(line)


if __name__ == "__main__":
    from mesh_generator.bin.call_psi_mesh_tool import create_psi_mesh
    from tests.ar_test import *

    create_psi_mesh(adjust__mesh_r_5().json_dict(), legacy__mesh_r_5().json_dict(), 'r',
                    "../mesh_generator/bin/", output_file_name="tmp_mesh_r.dat",
                    mesh_res_file_name="mesh_res_r.dat", show_plot=True, input_mesh=get_mesh_r_5().json_dict())


    i = 5
    path1 = '../mesh_generator/bin/'
    path2 = 'output/'
    duplicate_txt_file(path1 + "tmp_mesh_r.dat", path2 + "output02_mesh_r_" + str(i) + ".dat")
    duplicate_txt_file(path1 + "mesh_res_r.dat", "py_mesh_res/mesh_res_r_" + str(i) + ".dat")

    # save_test(str(path1) + "temp.py", "phi_test_38_adj", adjust__mesh_phi_38().__str__())
    # save_test(str(path1) + "temp.py", "phi_test_38_legacy", legacy__mesh_phi_38().__str__())
