"""
Unittest- All tests need to start with the word 'test'. Compares with the dictionary results in in the file
test/test_results.py, checking both step #2 (adjust mesh segments) and step #3 (legacy mesh) of the mesh calculations.
Then will compare with the output.dat file saved in the folder "output" and test step #4
(calling fortran mesh and finding the optimal total number of points in mesh spacing).

Last update: Oct 9th, 2020.

Current tests: 17 Theta, 38 Phi, 5 Radial. => total 60.

Note: *All phi cases are periodic.
      *Best way to create a test is as the tests below or in the format used in ar_test.py (using 3 different functions)
      .It is important you save the step 2 (adjusted mesh) as a dictionary before computing the legacy mesh. This
      dictionary is needed for check_mesh_valid() which will check iteratively if the mesh is above user requests.
      *An example test can also be found in test/create_mesh.py.

Observations: Theta tests that have a decreasing first segment will result in a garbage first point in
mesh_res.dat fortran results.

"""

import unittest
from mesh_generator.src.mesh import Mesh
from mesh_generator.src.mesh_segment import MeshSegment
from test_results import *
from mesh_generator.bin.call_psi_mesh_tool import create_psi_mesh
import filecmp
import numpy
import os


class TestMeshCases(unittest.TestCase):
    """ 50 unit tests testing python modules in the package mesh-generator. """

    def test_theta_1(self):
        """
        As expected! Results should not change after debugging.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh1 = Mesh(0.00, numpy.pi, False)
        mesh1.insert_mesh_segment(MeshSegment(1.10, 1.40, 0.01))
        mesh1.insert_mesh_segment(MeshSegment(1.30, 1.90, 0.02))
        mesh1.insert_mesh_segment(MeshSegment(0.40, 2.80, 0.04))
        if mesh1.is_valid():
            input_mesh = mesh1.json_dict()
            adj_mesh = mesh1.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], numpy.pi)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(adj_mesh["phi_shift"], 0.0)
            self.assertEqual(theta_test_1_adj, adj_mesh)
            legacy_mesh = mesh1.build_legacy_mesh().json_dict()
            self.assertEqual(theta_test_1_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 't', "../mesh_generator/bin/", output_file_name="tmp_mesh_t.dat",
                            mesh_res_file_name="mesh_res_t.dat", show_plot=False, input_mesh=input_mesh)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_t.dat",
                                        "output/output02_mesh_t_1.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_t.dat",
                                        "py_mesh_res/mesh_res_t_1.dat"))
        else:
            print("Theta test #1 mesh requirements are not valid.")

    def test_mesh_theta_2(self):
        """
        Test deletion of small segments. If segment contains less than 4 points it will be deleted.
        Results as expected.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.1
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh2 = Mesh(0.00, numpy.pi)
        mesh2.insert_mesh_segment(MeshSegment(1.367870, 1.405460, 0.000500))
        mesh2.insert_mesh_segment(MeshSegment(1.369750, 1.406870, 0.000750))
        mesh2.insert_mesh_segment(MeshSegment(1.323740, 1.438350, 0.002000))
        mesh2.insert_mesh_segment(MeshSegment(1.305580, 1.797560, 0.005000))
        mesh2.insert_mesh_segment(MeshSegment(1.268290, 1.910090, 0.010000))
        mesh2.insert_mesh_segment(MeshSegment(0.873363, 2.123720, 0.020000))
        mesh2.insert_mesh_segment(MeshSegment(1.225220, 1.445130, 0.015000))
        if mesh2.is_valid():
            input_mesh = mesh2.json_dict()
            adj_mesh = mesh2.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], numpy.pi)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(adj_mesh["phi_shift"], 0.0)
            self.assertEqual(theta_test_2_adj, adj_mesh)
            legacy_mesh = mesh2.build_legacy_mesh().json_dict()
            self.assertEqual(theta_test_2_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 't', "../mesh_generator/bin/", output_file_name="tmp_mesh_t.dat",
                            mesh_res_file_name="mesh_res_t.dat", show_plot=False, input_mesh=input_mesh)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_t.dat",
                                        "output/output02_mesh_t_2.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_t.dat",
                                        "py_mesh_res/mesh_res_t_2.dat"))
        else:
            print("Theta test #2 mesh requirements are not valid.")

    def test_mesh_theta_3(self):
        """
        Testing the overshoot case. Results are as expected.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh3 = Mesh(0.00, numpy.pi)
        mesh3.insert_mesh_segment(MeshSegment(1, 3.1, 0.04))
        mesh3.insert_mesh_segment(MeshSegment(2, 3, 0.01))
        mesh3.insert_mesh_segment(MeshSegment(1.5, 2.5, 0.02))

        if mesh3.is_valid():
            adj_mesh = mesh3.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], numpy.pi)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(adj_mesh["phi_shift"], 0.0)
            self.assertEqual(theta_test_3_adj, adj_mesh)
            legacy_mesh = mesh3.build_legacy_mesh().json_dict()
            self.assertEqual(theta_test_3_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 't', "../mesh_generator/bin/", output_file_name="tmp_mesh_t.dat",
                            mesh_res_file_name="mesh_res_t.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_t.dat",
                                        "output/output02_mesh_t_3.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_t.dat",
                                        "py_mesh_res/mesh_res_t_3.dat"))

        else:
            print("Theta test #3 mesh requirements are not valid.")

    def test_mesh_theta_4(self):
        """
        Testing resolve gap. Results as expected.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh4 = Mesh(0.00, numpy.pi, False)
        mesh4.insert_mesh_segment(MeshSegment(1, 1.5, 0.033))
        mesh4.insert_mesh_segment(MeshSegment(2, 2.5, 0.033))
        mesh4.insert_mesh_segment(MeshSegment(0.5, 0.7, 0.02))

        if mesh4.is_valid():
            adj_mesh = mesh4.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], numpy.pi)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(adj_mesh["phi_shift"], 0.0)
            self.assertEqual(theta_test_4_adj, adj_mesh)
            legacy_mesh = mesh4.build_legacy_mesh().json_dict()
            self.assertEqual(theta_test_4_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 't', "../mesh_generator/bin/", output_file_name="tmp_mesh_t.dat",
                            mesh_res_file_name="mesh_res_t.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_t.dat",
                                        "output/output02_mesh_t_4.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_t.dat",
                                        "py_mesh_res/mesh_res_t_4.dat"))

        else:
            print("Theta test #4 mesh requirements are not valid.")

    def test_theta_5(self):
        """
        As expected! Results should not change after debugging.
        Testing resolve_gap case 2.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh5 = Mesh(0.00, numpy.pi, False)
        mesh5.insert_mesh_segment(MeshSegment(1, 1.5, 0.055))
        mesh5.insert_mesh_segment(MeshSegment(2, 2.5, 0.035))
        if mesh5.is_valid():
            adj_mesh = mesh5.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], numpy.pi)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(adj_mesh["phi_shift"], 0.0)
            self.assertEqual(theta_test_5_adj, adj_mesh)
            legacy_mesh = mesh5.build_legacy_mesh().json_dict()
            self.assertEqual(theta_test_5_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 't', "../mesh_generator/bin/", output_file_name="tmp_mesh_t.dat",
                            mesh_res_file_name="mesh_res_t.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_t.dat",
                                        "output/output02_mesh_t_5.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_t.dat",
                                        "py_mesh_res/mesh_res_t_5.dat"))
        else:
            print("Theta test #5 mesh requirements are not valid.")

    def test_theta_6(self):
        """
        As expected! Because of first segment being constant then there is a shift.
        First point in mesh_res.dat is garbage.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.09
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh6 = Mesh(0.00, numpy.pi, False)
        mesh6.insert_mesh_segment(MeshSegment(1, 1.5, 0.07, var_ds_ratio=1.09))
        mesh6.insert_mesh_segment(MeshSegment(2, 2.5, 0.01))
        if mesh6.is_valid():
            adj_mesh = mesh6.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], numpy.pi)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(adj_mesh["phi_shift"], 0.0)
            self.assertEqual(theta_test_6_adj, adj_mesh)
            legacy_mesh = mesh6.build_legacy_mesh().json_dict()
            self.assertEqual(theta_test_6_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 't', "../mesh_generator/bin/", output_file_name="tmp_mesh_t.dat",
                            mesh_res_file_name="mesh_res_t.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_t.dat",
                                        "output/output02_mesh_t_6.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_t.dat",
                                        "py_mesh_res/mesh_res_t_6.dat"))

        else:
            print("Theta test #6 mesh requirements are not valid.")

    def test_theta_7(self):
        """As expected! """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh7 = Mesh(0.00, numpy.pi, False)
        mesh7.insert_mesh_segment(MeshSegment(1, 1.2, 0.02))
        mesh7.insert_mesh_segment(MeshSegment(2, 2.5, 0.2))
        mesh7.insert_mesh_segment(MeshSegment(1.5, 2.5, 0.03))
        mesh7.insert_mesh_segment(MeshSegment(2.4, 3, 0.01))
        mesh7.insert_mesh_segment(MeshSegment(0, 0.4, 0.03))
        mesh7.insert_mesh_segment(MeshSegment(3, numpy.pi, 0.1))
        if mesh7.is_valid():
            adj_mesh = mesh7.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], numpy.pi)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(adj_mesh["phi_shift"], 0.0)
            self.assertEqual(theta_test_7_adj, adj_mesh)
            legacy_mesh = mesh7.build_legacy_mesh().json_dict()
            self.assertEqual(theta_test_7_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 't', "../mesh_generator/bin/", output_file_name="tmp_mesh_t.dat",
                            mesh_res_file_name="mesh_res_t.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_t.dat",
                                        "output/output02_mesh_t_7.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_t.dat",
                                        "py_mesh_res/mesh_res_t_7.dat"))

        else:
            print("Theta test #7 mesh requirements are not valid.")

    def test_theta_8(self):
        """
        Results is as expected.  This is an edge case when the mesh resulted in a mesh with low total number of points, this
        is when running check_mesh_valid iterative method is important. Here if you add one point to the total mesh
        number the mesh will be below user requests, and will solve the problem here.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh8 = Mesh(0.00, numpy.pi, False)
        mesh8.insert_mesh_segment(MeshSegment(0.5, 1.2, 0.2))
        mesh8.insert_mesh_segment(MeshSegment(2, 3, 0.2))
        if mesh8.is_valid():
            adj_mesh = mesh8.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], numpy.pi)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(adj_mesh["phi_shift"], 0.0)
            self.assertEqual(theta_test_8_adj, adj_mesh)
            legacy_mesh = mesh8.build_legacy_mesh().json_dict()
            self.assertEqual(theta_test_8_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 't', "../mesh_generator/bin/", output_file_name="tmp_mesh_t.dat",
                            mesh_res_file_name="mesh_res_t.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_t.dat",
                                        "output/output02_mesh_t_8.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_t.dat",
                                        "py_mesh_res/mesh_res_t_8.dat"))

        else:
            print("Theta test #8 mesh requirements are not valid.")

    def test_theta_9(self):
        """
        Results as expected.
        This test is formed by Cooper using the Mesh UI (easy way to create edge cases and save them as dictionaries).
        The bug was in check_mesh_valid - function to iteratively optimize the total number of points in the mesh.
        -DEBUG: RESOLVED.
         """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.1
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh9 = Mesh(0.00, numpy.pi, False)
        mesh9.insert_mesh_segment(MeshSegment(1.1356989345533366, 1.5916809800055658, 0.01))
        mesh9.insert_mesh_segment(MeshSegment(1.3532502198056797, 1.4283274775004602, 0.001))
        mesh9.insert_mesh_segment(MeshSegment(0.8473725097896048, 2.06673071910115, 0.02))
        if mesh9.is_valid():
            adj_mesh = mesh9.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], numpy.pi)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(adj_mesh["phi_shift"], 0.0)
            self.assertEqual(theta_test_9_adj, adj_mesh)
            legacy_mesh = mesh9.build_legacy_mesh().json_dict()
            self.assertEqual(theta_test_9_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 't', "../mesh_generator/bin/", output_file_name="tmp_mesh_t.dat",
                            mesh_res_file_name="mesh_res_t.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_t.dat",
                                        "output/output02_mesh_t_9.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_t.dat",
                                        "py_mesh_res/mesh_res_t_9.dat"))

        else:
            print("Theta test #9 mesh requirements are not valid.")

    def test_theta_10(self):
        """Results as expected. The whole mesh should be below a certain ds. """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.1
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh10 = Mesh(0.00, numpy.pi, False)
        mesh10.insert_mesh_segment(MeshSegment(0, numpy.pi, 0.08, var_ds_ratio=1.1))
        mesh10.insert_mesh_segment(MeshSegment(1.3, 1.42, 0.001))
        if mesh10.is_valid():
            adj_mesh = mesh10.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], numpy.pi)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(adj_mesh["phi_shift"], 0.0)
            self.assertEqual(theta_test_10_adj, adj_mesh)
            legacy_mesh = mesh10.build_legacy_mesh().json_dict()
            self.assertEqual(theta_test_10_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 't', "../mesh_generator/bin/", output_file_name="tmp_mesh_t.dat",
                            mesh_res_file_name="mesh_res_t.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_t.dat",
                                        "output/output02_mesh_t_10.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_t.dat",
                                        "py_mesh_res/mesh_res_t_10.dat"))
        else:
            print("Theta test #10 mesh requirements are not valid.")

    def test_theta_11(self):
        """
        The simplest mesh. One mesh constant segment across the mesh region.
        Results as expected.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh11 = Mesh(0.00, numpy.pi, False)
        mesh11.insert_mesh_segment(MeshSegment(0, numpy.pi, 0.03))
        if mesh11.is_valid():
            adj_mesh = mesh11.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], numpy.pi)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(adj_mesh["phi_shift"], 0.0)
            self.assertEqual(theta_test_11_adj, adj_mesh)
            legacy_mesh = mesh11.build_legacy_mesh().json_dict()
            self.assertEqual(theta_test_11_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 't', "../mesh_generator/bin/", output_file_name="tmp_mesh_t.dat",
                            mesh_res_file_name="mesh_res_t.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_t.dat",
                                        "output/output02_mesh_t_11.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_t.dat",
                                        "py_mesh_res/mesh_res_t_11.dat"))
        else:
            print("Warning: Theta test 11 is not valid. ")

    def test_theta_12(self):
        """
        Back to back undershooting segments. This test was created using the UI. This test called to create the
        function "update_next_segment(segment, i, legacy_segment)". Results are now as expected.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.02
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.04
        mesh12 = Mesh(0.00, numpy.pi, False)
        mesh12.insert_mesh_segment(MeshSegment(0.3953275896083537, 0.7317766020409957, 0.01))
        mesh12.insert_mesh_segment(MeshSegment(1.2364501206899585, 1.6906562874740247, 0.07))
        mesh12.insert_mesh_segment(MeshSegment(1.8168246671362658, 2.61589107166379, 0.09))
        if mesh12.is_valid():
            adj_mesh = mesh12.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], numpy.pi)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(adj_mesh["phi_shift"], 0.0)
            self.assertEqual(theta_test_12_adj, adj_mesh)
            legacy_mesh = mesh12.build_legacy_mesh().json_dict()
            self.assertEqual(theta_test_12_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 't', "../mesh_generator/bin/", output_file_name="tmp_mesh_t.dat",
                            mesh_res_file_name="mesh_res_t.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_t.dat",
                                        "output/output02_mesh_t_12.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_t.dat",
                                        "py_mesh_res/mesh_res_t_12.dat"))
        else:
            print("Warning: Theta test 12 is not valid. ")

    def test_theta_13(self):
        """
        Testing back to back increasing then decreasing fixed ds segments. Results are as expected.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.09
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.04
        mesh13 = Mesh(0.00, numpy.pi, False)
        mesh13.insert_mesh_segment(MeshSegment(0.4, 1.1, 0.07))
        mesh13.insert_mesh_segment(MeshSegment(0.1, 0.4, 0.03))
        mesh13.insert_mesh_segment(MeshSegment(1.1, 1.8, 0.03))
        mesh13.insert_mesh_segment(MeshSegment(1.8, 2.3, 0.06))
        mesh13.insert_mesh_segment(MeshSegment(2.3, 2.8, 0.02))
        if mesh13.is_valid():
            adj_mesh = mesh13.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], numpy.pi)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(adj_mesh["phi_shift"], 0.0)
            self.assertEqual(theta_test_13_adj, adj_mesh)
            legacy_mesh = mesh13.build_legacy_mesh().json_dict()
            self.assertEqual(theta_test_13_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 't', "../mesh_generator/bin/", output_file_name="tmp_mesh_t.dat",
                            mesh_res_file_name="mesh_res_t.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_t.dat",
                                        "output/output02_mesh_t_13.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_t.dat",
                                        "py_mesh_res/mesh_res_t_13.dat"))
        else:
            print("Warning: Theta test 13 is not valid. ")

    def test_theta_14(self):
        """
        Recalculate since there is a discontinuity in mesh. Legacy iteration.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.02
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.04
        mesh14 = Mesh(0.00, numpy.pi, False)
        mesh14.insert_mesh_segment(MeshSegment(0.3953275896083537, 0.7317766020409957, 0.09))
        mesh14.insert_mesh_segment(MeshSegment(1.2364501206899585, 1.6906562874740247, 0.07))
        mesh14.insert_mesh_segment(MeshSegment(1.8168246671362658, 2.61589107166379, 0.01))
        if mesh14.is_valid():
            adj_mesh = mesh14.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], numpy.pi)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(adj_mesh["phi_shift"], 0.0)
            self.assertEqual(theta_test_14_adj, adj_mesh)
            legacy_mesh = mesh14.build_legacy_mesh().json_dict()
            self.assertEqual(theta_test_14_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 't', "../mesh_generator/bin/", output_file_name="tmp_mesh_t.dat",
                            mesh_res_file_name="mesh_res_t.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_t.dat",
                                        "output/output02_mesh_t_14.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_t.dat",
                                        "py_mesh_res/mesh_res_t_14.dat"))
        else:
            print("Warning: Theta test 14 is not valid. ")

    def test_theta_15(self):
        """
        If first segment is fixed ds and legacy_length > segment_length then convert to fixed length segment.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.07
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
        mesh15 = Mesh(0.00, numpy.pi, False)
        mesh15.insert_mesh_segment(MeshSegment(0., 0.73, 0.06))
        mesh15.insert_mesh_segment(MeshSegment(1.2, 1.690, 0.001))
        if mesh15.is_valid():
            adj_mesh = mesh15.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], numpy.pi)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(adj_mesh["phi_shift"], 0.0)
            self.assertEqual(theta_test_15_adj, adj_mesh)
            legacy_mesh = mesh15.build_legacy_mesh().json_dict()
            self.assertEqual(theta_test_15_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 't', "../mesh_generator/bin/", output_file_name="tmp_mesh_t.dat",
                            mesh_res_file_name="mesh_res_t.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_t.dat",
                                        "output/output02_mesh_t_15.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_t.dat",
                                        "py_mesh_res/mesh_res_t_15.dat"))
        else:
            print("Warning: Theta test 15 is not valid. ")

    def test_theta_16(self):
        """
        The symmetric case of theta test #15.
        If last segment is fixed ds and legacy_length > segment_length then convert to fixed length segment.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.07
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
        mesh16 = Mesh(0.00, numpy.pi, False)
        mesh16.insert_mesh_segment(MeshSegment(1.9, 2.3, 0.001))
        mesh16.insert_mesh_segment(MeshSegment(2.5, numpy.pi, 0.07))
        if mesh16.is_valid():
            adj_mesh = mesh16.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], numpy.pi)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(adj_mesh["phi_shift"], 0.0)
            self.assertEqual(theta_test_16_adj, adj_mesh)
            legacy_mesh = mesh16.build_legacy_mesh().json_dict()
            self.assertEqual(theta_test_16_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 't', "../mesh_generator/bin/", output_file_name="tmp_mesh_t.dat",
                            mesh_res_file_name="mesh_res_t.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_t.dat",
                                        "output/output02_mesh_t_16.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_t.dat",
                                        "py_mesh_res/mesh_res_t_16.dat"))
        else:
            print("Warning: Theta test 16 is not valid. ")

    def test_theta_17(self):
        """
        This is a previous bug Cooper found using the UI. In this theta case,
        the ratio of the last segment does not match the var_ds_ratio constraint.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.04
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh17 = Mesh(0.00, numpy.pi, False)
        mesh17.insert_mesh_segment(MeshSegment(1.242461236299751, 1.3403353414879318, 0.001))
        mesh17.insert_mesh_segment(MeshSegment(1.1164846652654585, 1.4469309015938716, 0.0015))
        mesh17.insert_mesh_segment(MeshSegment(0.5193418749836787, 2.009257090100789, 0.02))
        mesh17.insert_mesh_segment(MeshSegment(0.1, 3.04, 0.1))
        mesh17.insert_mesh_segment(MeshSegment(1.465914082875949, 2.115869280025328, 0.047))

        if mesh17.is_valid():
            adj_mesh = mesh17.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], numpy.pi)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(adj_mesh["phi_shift"], 0.0)
            self.assertEqual(theta_test_17_adj, adj_mesh)
            legacy_mesh = mesh17.build_legacy_mesh().json_dict()
            self.assertEqual(theta_test_17_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 't', "../mesh_generator/bin/",
                            output_file_name="tmp_mesh_t.dat",
                            mesh_res_file_name="mesh_res_t.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_t.dat",
                                        "output/output02_mesh_t_17.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_t.dat",
                                        "py_mesh_res/mesh_res_t_17.dat"))
        else:
            print("Warning: Theta test 17 is not valid. ")

    def test_phi_1(self):
        """ Results are as expected."""
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh1 = Mesh(0.00, 2 * numpy.pi, True)
        mesh1.insert_mesh_segment(MeshSegment(3.0, 3.3, 0.01))
        mesh1.insert_mesh_segment(MeshSegment(2.8, 3.8, 0.02))
        mesh1.insert_mesh_segment(MeshSegment(0.4, 2 * numpy.pi - 0.3, 0.04))
        if mesh1.is_valid():
            adj_mesh = mesh1.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_1_adj, adj_mesh)
            legacy_mesh = mesh1.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_1_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)

            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_1.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_1.dat"))
        else:
            print("Phi test #1 mesh requirements are not valid.")

    def test_phi_2(self):
        """Results as expected. """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh2 = Mesh(0.00, 2 * numpy.pi, True)
        mesh2.insert_mesh_segment(MeshSegment(0.0, 3.3, 0.01))
        mesh2.insert_mesh_segment(MeshSegment(2.8, 3.8, 0.02))
        mesh2.insert_mesh_segment(MeshSegment(3, 4.5, 0.04))

        if mesh2.is_valid():
            adj_mesh = mesh2.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_2_adj, adj_mesh)
            legacy_mesh = mesh2.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_2_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_2.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_2.dat"))
        else:
            print("Phi test #2 mesh requirements are not valid.")

    def test_phi_3(self):
        """Results as expected.  """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh3 = Mesh(0.00, 2 * numpy.pi, True)
        mesh3.insert_mesh_segment(MeshSegment(1, 5.5, 0.02))
        mesh3.insert_mesh_segment(MeshSegment(2.2, 3, 0.03))
        mesh3.insert_mesh_segment(MeshSegment(3.01, 4.5, 0.05))

        if mesh3.is_valid():
            adj_mesh = mesh3.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_3_adj, adj_mesh)
            legacy_mesh = mesh3.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_3_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_3.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_3.dat"))
        else:
            print("Phi test #3 mesh requirements are not valid.")

    def test_phi_4(self):
        """Results as expected.  """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh4 = Mesh(0.00, 2 * numpy.pi, True)
        mesh4.insert_mesh_segment(MeshSegment(3.01, 3.03, 0.02))
        mesh4.insert_mesh_segment(MeshSegment(3.01, 4.5, 0.05))
        mesh4.insert_mesh_segment(MeshSegment(4, 6.25, 0.01))

        if mesh4.is_valid():
            adj_mesh = mesh4.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_4_adj, adj_mesh)
            legacy_mesh = mesh4.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_4_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_4.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_4.dat"))
        else:
            print("Phi test #4 mesh requirements are not valid.")

    def test_phi_5(self):
        """Results as expected.  """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh5 = Mesh(0.00, 2 * numpy.pi, True)
        mesh5.insert_mesh_segment(MeshSegment(0.5, 1.1, 0.09))
        mesh5.insert_mesh_segment(MeshSegment(0.7, 1.3, 0.07))

        if mesh5.is_valid():
            adj_mesh = mesh5.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_5_adj, adj_mesh)
            legacy_mesh = mesh5.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_5_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_5.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_5.dat"))
        else:
            print("Phi test #5 mesh requirements are not valid.")

    def test_phi_6(self):
        """Results as expected.  """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh6 = Mesh(0.00, 2 * numpy.pi, True)
        mesh6.insert_mesh_segment(MeshSegment(0, 1.1, 0.03))
        mesh6.insert_mesh_segment(MeshSegment(2, 3, 0.07))
        mesh6.insert_mesh_segment(MeshSegment(2, 6.1, 0.05))
        mesh6.insert_mesh_segment(MeshSegment(6, 2 * numpy.pi, 0.01))

        if mesh6.is_valid():
            adj_mesh = mesh6.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_6_adj, adj_mesh)
            legacy_mesh = mesh6.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_6_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_6.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_6.dat"))
        else:
            print("Phi test #6 mesh requirements are not valid.")

    #
    def test_phi_7(self):
        """Results as expected. Periodic and resolve gap mesh segment. """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh7 = Mesh(0.00, 2 * numpy.pi, True)
        mesh7.insert_mesh_segment(MeshSegment(1, 1.8, 0.03))

        if mesh7.is_valid():
            adj_mesh = mesh7.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_7_adj, adj_mesh)
            legacy_mesh = mesh7.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_7_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_7.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_7.dat"))
        else:
            print("Phi test #7 mesh requirements are not valid.")

    def test_phi_8(self):
        """Results as expected. Periodic and resolve gap mesh segment. This is when we have the begin and end segments
         being fixed ds. Then we want to only change the end segment to be like gen_solve.  """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh8 = Mesh(0.00, 2 * numpy.pi, True)
        mesh8.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.08, var_ds_ratio=1.06))
        mesh8.insert_mesh_segment(MeshSegment(1, 1.8, 0.01))
        mesh8.insert_mesh_segment(MeshSegment(1.4, 1.8, 0.001))
        mesh8.insert_mesh_segment(MeshSegment(0.0, 3.8, 0.02))
        if mesh8.is_valid():
            adj_mesh = mesh8.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_8_adj, adj_mesh)
            legacy_mesh = mesh8.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_8_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_8.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_8.dat"))
        else:
            print("Phi test #8 mesh requirements are not valid.")

    def test_phi_9(self):
        """
        Make periodic: here begin and end segments are fixed ds. Now it will only change the begin segment
        same logic as resolve gap.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh9 = Mesh(0.00, 2 * numpy.pi, True)
        mesh9.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.1, var_ds_ratio=1.06))
        mesh9.insert_mesh_segment(MeshSegment(2.4, 4.8, 0.001))
        mesh9.insert_mesh_segment(MeshSegment(4.7, 2 * numpy.pi, 0.01))

        if mesh9.is_valid():
            adj_mesh = mesh9.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_9_adj, adj_mesh)
            legacy_mesh = mesh9.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_9_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_9.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_9.dat"))
        else:
            print("Phi test #9 mesh requirements are not valid.")

    def test_phi_10(self):
        """
        Make periodic: begin and end segments are fixed ds. Now it will only change the begin segment
        same logic as resolve gap. here we also have a constant segment added to begin segment so it will still
        follow user request.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh10 = Mesh(0.00, 2 * numpy.pi, True)
        mesh10.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.06, var_ds_ratio=1.06))
        mesh10.insert_mesh_segment(MeshSegment(2.4, 4.8, 0.001))
        mesh10.insert_mesh_segment(MeshSegment(4.7, 2 * numpy.pi, 0.01))
        if mesh10.is_valid():
            adj_mesh = mesh10.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_10_adj, adj_mesh)
            legacy_mesh = mesh10.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_10_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_10.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_10.dat"))
        else:
            print("Phi test #10 mesh requirements are not valid.")

    def test_phi_11(self):
        """
        Fixed ds on both ends when begin.ds0 = end.ds1. Handled like fixed length on both sides.
        Results are as expected.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.05
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh11 = Mesh(0.00, 2 * numpy.pi, True)
        mesh11.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.07, var_ds_ratio=1.04))
        mesh11.insert_mesh_segment(MeshSegment(2.4, 4.8, 0.002))
        mesh11.insert_mesh_segment(MeshSegment(3.4, 4.1, 0.001))
        mesh11.insert_mesh_segment(MeshSegment(4.7, 5.5, 0.01))
        mesh11.insert_mesh_segment(MeshSegment(2, 3, 0.01))
        if mesh11.is_valid():
            adj_mesh = mesh11.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_11_adj, adj_mesh)
            legacy_mesh = mesh11.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_11_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_11.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_11.dat"))
        else:
            print("Phi test #11 mesh requirements are not valid.")

    def test_phi_12(self):
        """ Fixed ds on both ends when begin.ds0 = end.ds1. add a constant segment to the end segment. """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.05
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh12 = Mesh(0.00, 2 * numpy.pi, True)
        mesh12.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.05, var_ds_ratio=1.04))
        mesh12.insert_mesh_segment(MeshSegment(3.4, 4.1, 0.001))
        mesh12.insert_mesh_segment(MeshSegment(4.7, 5.5, 0.01))
        mesh12.insert_mesh_segment(MeshSegment(2, 3, 0.01))
        if mesh12.is_valid():
            adj_mesh = mesh12.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_12_adj, adj_mesh)
            legacy_mesh = mesh12.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_12_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_12.dat"))

        else:
            print("Phi test #12 mesh requirements are not valid.")

    def test_phi_13(self):
        """ Fixed ds on both ends when begin.ds0 = end.ds1. add a constant segment to the end segment. """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.05
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh13 = Mesh(0.00, 2 * numpy.pi, True)
        mesh13.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.04, var_ds_ratio=1.04))
        mesh13.insert_mesh_segment(MeshSegment(3.4, 4.1, 0.03))
        mesh13.insert_mesh_segment(MeshSegment(4.7, 5.5, 0.01))
        mesh13.insert_mesh_segment(MeshSegment(2, 3, 0.01))
        if mesh13.is_valid():
            adj_mesh = mesh13.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_13_adj, adj_mesh)
            legacy_mesh = mesh13.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_13_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_13.dat"))

        else:
            print("Phi test #13 mesh requirements are not valid.")

    def test_phi_14(self):
        """ undershoot case. Results are as expected. Complex mesh. """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh14 = Mesh(0.00, 2 * numpy.pi, True)
        mesh14.insert_mesh_segment(MeshSegment(1.4130858522170961, 2.1280400036364604, 0.03))
        mesh14.insert_mesh_segment(MeshSegment(0.9504684601222136, 2.237385932677069, 0.05))
        mesh14.insert_mesh_segment(MeshSegment(1.514020555946889, 1.7327124140281063, 0.001))
        if mesh14.is_valid():
            adj_mesh = mesh14.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_14_adj, adj_mesh)
            legacy_mesh = mesh14.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_14_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_14.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_14.dat"))
        else:
            print("Phi test #14 mesh requirements are not valid.")

    def test_phi_15(self):
        """ Added to cover all permutations of fixed_ds in begin and end segments periodic function.  """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.09
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh15 = Mesh(0.00, 2 * numpy.pi, True)
        mesh15.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.09, var_ds_ratio=1.06))
        mesh15.insert_mesh_segment(MeshSegment(3.4, 4.1, 0.03))
        mesh15.insert_mesh_segment(MeshSegment(2, 2.2, 0.001))
        mesh15.insert_mesh_segment(MeshSegment(0, 3, 0.01))
        if mesh15.is_valid():
            adj_mesh = mesh15.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_15_adj, adj_mesh)
            legacy_mesh = mesh15.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_15_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_15.dat"))
        else:
            print("Phi test #15 mesh requirements are not valid.")

    def test_phi_16(self):
        """
        Results as expected. Simplest mesh containing one constant segment across mesh region.
        Ratio should be 1.0.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.09
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh16 = Mesh(0.00, 2 * numpy.pi, True)
        mesh16.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.01))
        if mesh16.is_valid():
            adj_mesh = mesh16.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_16_adj, adj_mesh)
            legacy_mesh = mesh16.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_16_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_16.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_16.dat"))
        else:
            print("Phi test #16 mesh requirements are not valid.")

    def test_phi_17(self):
        """ Results as expected. Adjusting the FG REGION RATIO."""
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.04
        mesh17 = Mesh(0.00, 2 * numpy.pi, True)
        mesh17.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.03))
        mesh17.insert_mesh_segment(MeshSegment(4.886921905584123, 5.467296452030431, 0.01))
        mesh17.insert_mesh_segment(MeshSegment(2.052338975839116, 3.3308452230831547, 0.02))
        if mesh17.is_valid():
            adj_mesh = mesh17.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_17_adj, adj_mesh)
            legacy_mesh = mesh17.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_17_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_17.dat"))

        else:
            print("Phi test #17 mesh requirements are not valid.")

    def test_phi_18(self):
        """ Results as expected. Adjusting the FG REGION RATIO."""
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.04
        mesh18 = Mesh(0.00, 2 * numpy.pi, True)
        mesh18.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.02))
        mesh18.insert_mesh_segment(MeshSegment(3.3476676737047866, 3.6925279114482451, 0.001))
        mesh18.insert_mesh_segment(MeshSegment(2.472900241379918, 3.843929967042934, 0.01))
        if mesh18.is_valid():
            adj_mesh = mesh18.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_18_adj, adj_mesh)
            legacy_mesh = mesh18.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_18_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_18.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_18.dat"))
        else:
            print("Phi test #18 mesh requirements are not valid.")

    def test_phi_19(self):
        """ Results as expected. Adjusting the FG REGION RATIO."""
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.05
        mesh19 = Mesh(0.00, 2 * numpy.pi, True)
        mesh19.insert_mesh_segment(MeshSegment(3.3476676737047866, 3.6925279114482451, 0.001))
        mesh19.insert_mesh_segment(MeshSegment(2.472900241379918, 3.843929967042934, 0.04))
        mesh19.insert_mesh_segment(MeshSegment(4, 4.5, 0.04))
        if mesh19.is_valid():
            adj_mesh = mesh19.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_19_adj, adj_mesh)
            legacy_mesh = mesh19.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_19_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_19.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_19.dat"))
        else:
            print("Phi test #19 mesh requirements are not valid.")

    def test_phi_20(self):
        """ Results as expected. Recalculate phi_shift because the end segment is undershooting."""
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.02
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.04
        mesh20 = Mesh(0.00, 2 * numpy.pi, True)
        mesh20.insert_mesh_segment(MeshSegment(1.3037399231764875, 1.8168246671362667, 0.01))
        mesh20.insert_mesh_segment(MeshSegment(1.2532725713115913, 1.8672920190011628, 0.07))
        mesh20.insert_mesh_segment(MeshSegment(2.0186940745958517, 2.7252370007043996, 0.09))
        if mesh20.is_valid():
            adj_mesh = mesh20.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_20_adj, adj_mesh)
            legacy_mesh = mesh20.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_20_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_20.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_20.dat"))
        else:
            print("Phi test #20 mesh requirements are not valid.")

    def test_phi_21(self):
        """ Results as expected. Recalculate phi_shift because begin segment is undershooting."""
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.02
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.04
        mesh21 = Mesh(0.00, 2 * numpy.pi, True)
        mesh21.insert_mesh_segment(MeshSegment(1.3037399231764875, 1.8168246671362667, 0.09))
        mesh21.insert_mesh_segment(MeshSegment(1.2532725713115913, 1.8672920190011628, 0.07))
        mesh21.insert_mesh_segment(MeshSegment(2.0186940745958517, 2.7252370007043996, 0.01))
        if mesh21.is_valid():
            adj_mesh = mesh21.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_21_adj, adj_mesh)
            legacy_mesh = mesh21.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_21_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_21.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_21.dat"))

        else:
            print("Phi test #21 mesh requirements are not valid.")

    def test_phi_22(self):
        """
        Results as expected. Recalculate phi_shift because begin AND end segment is undershooting. phi_shift is
        called 3 times total. This is not ideal scenario but still possible.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.07
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.04
        mesh22 = Mesh(0.00, 2 * numpy.pi, True)
        mesh22.insert_mesh_segment(MeshSegment(1.3037399231764875, 1.8168246671362667, 0.09))
        mesh22.insert_mesh_segment(MeshSegment(2.0186940745958517, 2.7252370007043996, 0.01))
        mesh22.insert_mesh_segment(MeshSegment(1.2532725713115913, 3.8672920190011628, 0.07))
        if mesh22.is_valid():
            adj_mesh = mesh22.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_22_adj, adj_mesh)
            legacy_mesh = mesh22.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_22_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_22.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_22.dat"))
        else:
            print("Phi test #22 mesh requirements are not valid.")

    def test_phi_23(self):
        """
        Results as expected. Back to back undershooting segments.
        The function "update_next_segment(segment, i, legacy_segment)" is driven from this test.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
        mesh23 = Mesh(0.00, 2 * numpy.pi, True)
        mesh23.insert_mesh_segment(MeshSegment(0.3953275896083546, 1.3626185003522, 0.06))
        mesh23.insert_mesh_segment(MeshSegment(2.262619608609517, 2.8934615069207203, 0.01))
        mesh23.insert_mesh_segment(MeshSegment(3.3644901243264194, 4.163556528853944, 0.001))
        mesh23.insert_mesh_segment(MeshSegment(0.44579494147325105, 3.986920797326807, 0.08))
        mesh23.insert_mesh_segment(MeshSegment(2.5570124944880788, 2.7168257753935836, 0.001))
        if mesh23.is_valid():
            adj_mesh = mesh23.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_23_adj, adj_mesh)
            legacy_mesh = mesh23.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_23_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_23.dat"))

        else:
            print("Phi test #23 mesh requirements are not valid.")

    def test_phi_24(self):
        """
        Results as expected. Back to back undershooting segments.
        This test is created by Cooper using the UI. Saved in ui/test_2020_06_13_CD1_b2b_uniform_bug.json
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
        mesh24 = Mesh(0.00, 2 * numpy.pi, True)
        mesh24.insert_mesh_segment(MeshSegment(0.3953275896083546, 1.3626185003522, 0.06))
        mesh24.insert_mesh_segment(MeshSegment(2.262619608609517, 2.8934615069207203, 0.01))
        mesh24.insert_mesh_segment(MeshSegment(3.3644901243264194, 4.163556528853944, 0.001))
        mesh24.insert_mesh_segment(MeshSegment(0.44579494147325105, 3.986920797326807, 0.08))
        mesh24.insert_mesh_segment(MeshSegment(2.5570124944880788, 2.7168257753935836, 0.001))
        mesh24.insert_mesh_segment(MeshSegment(0, 2 * numpy.pi, 0.06))
        if mesh24.is_valid():
            adj_mesh = mesh24.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_24_adj, adj_mesh)
            legacy_mesh = mesh24.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_24_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_24.dat"))

        else:
            print("Phi test #24 mesh requirements are not valid.")

    def test_phi_25(self):
        """
        Needs debugging for recalculate periodic case. This is a test created by Cooper using the mesh UI.
        The problem here is the spike in ratio plot. This is because there is undershooting in adjacent segment to
        begin segment but the periodic case is not recalculated because it is difficult to revert back. This can be
        resolved in the build legacy step. **Think about these cases as a whole**.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
        mesh25 = Mesh(0.00, 2 * numpy.pi, True)
        mesh25.insert_mesh_segment(MeshSegment(0.3953275896083546, 1.3626185003522, 0.06))
        mesh25.insert_mesh_segment(MeshSegment(2.262619608609517, 2.8934615069207203, 0.01))
        mesh25.insert_mesh_segment(MeshSegment(3.3644901243264194, 4.163556528853944, 0.001))
        mesh25.insert_mesh_segment(MeshSegment(0.44579494147325105, 3.986920797326807, 0.08))
        mesh25.insert_mesh_segment(MeshSegment(2.5570124944880788, 2.7168257753935836, 0.001))
        mesh25.insert_mesh_segment(MeshSegment(0, 2 * numpy.pi, 0.07))
        if mesh25.is_valid():
            adj_mesh = mesh25.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_25_adj, adj_mesh)
            legacy_mesh = mesh25.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_25_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_25.dat"))

        else:
            print("Phi test #25 mesh requirements are not valid.")

    def test_phi_26(self):
        """
        Slight tweek in phi test 25.
        Here it is as expected. I test the case when the periodic case does not reach an intersection so there is a need
        to delete the last segment and extend the begin segment to continue to grow as much as possible.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
        mesh26 = Mesh(0.00, 2 * numpy.pi, True)
        mesh26.insert_mesh_segment(MeshSegment(0.3953275896083546, 1.3626185003522, 0.001))
        mesh26.insert_mesh_segment(MeshSegment(2.262619608609517, 2.8934615069207203, 0.01))
        mesh26.insert_mesh_segment(MeshSegment(3.3644901243264194, 4.163556528853944, 0.06))
        mesh26.insert_mesh_segment(MeshSegment(0.44579494147325105, 3.986920797326807, 0.08))
        mesh26.insert_mesh_segment(MeshSegment(2.5570124944880788, 2.7168257753935836, 0.001))
        mesh26.insert_mesh_segment(MeshSegment(0, 2 * numpy.pi, 0.07))
        if mesh26.is_valid():
            adj_mesh = mesh26.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_26_adj, adj_mesh)
            legacy_mesh = mesh26.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_26_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_26.dat"))

        else:
            print("Phi test #26 mesh requirements are not valid.")

    def test_phi_27(self):
        """
        The mirror of test 26. Here the segment_end.ds1 is much bigger that segment_begin.ds0 so there is no resolve
        gap sh intersection in this region. Therefore, we delete the the begin segment and extend the last segment
        to grow as much as possible.
        Results are as expected.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
        mesh27 = Mesh(0.00, 2 * numpy.pi, True)
        mesh27.insert_mesh_segment(MeshSegment(0.3953275896083546, 1.3626185003522, 0.001))
        mesh27.insert_mesh_segment(MeshSegment(3.3644901243264194, 4.163556528853944, 0.06))
        mesh27.insert_mesh_segment(MeshSegment(3.3644901243264194, 5.163556528853944, 0.09))
        if mesh27.is_valid():
            adj_mesh = mesh27.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_27_adj, adj_mesh)
            legacy_mesh = mesh27.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_27_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_27.dat"))

        else:
            print("Phi test #27 mesh requirements are not valid.")

    def test_phi_28(self):
        """
        A very simple test of when the intersection sh is not in the region. Periodic case deletes the begin segment
        and extends the last segment.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.04
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
        mesh28 = Mesh(0.00, 2 * numpy.pi, True)
        mesh28.insert_mesh_segment(MeshSegment(0.753275896083546, 1.3626185003522, 0.12))
        mesh28.insert_mesh_segment(MeshSegment(4.1644901243264194, 4.363556528853944, 0.0001))
        if mesh28.is_valid():
            adj_mesh = mesh28.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_28_adj, adj_mesh)
            legacy_mesh = mesh28.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_28_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_28.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_p.dat",
                                        "fortran_mesh_res/mesh_res_p_28.dat"))
        else:
            print("Phi test #28 mesh requirements are not valid.")

    def test_phi_29(self):
        """
        Periodic case recalculated when the first time there was a case of adding a constant mesh segment because the
        original mesh required the begin and end segments to be fixed ds.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
        mesh29 = Mesh(0.00, 2 * numpy.pi, True)
        mesh29.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.03))
        mesh29.insert_mesh_segment(MeshSegment(4.886921905584123, 5.467296452030431, 0.01))
        mesh29.insert_mesh_segment(MeshSegment(3.886921905584123, 4.467296452030431, 0.0001))
        mesh29.insert_mesh_segment(MeshSegment(3.052338975839116, 3.3308452230831547, 0.02))
        if mesh29.is_valid():
            adj_mesh = mesh29.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_29_adj, adj_mesh)
            legacy_mesh = mesh29.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_29_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_29.dat"))

        else:
            print("Phi test #29 mesh requirements are not valid.")

    def test_phi_30(self):
        """
        Periodic case where segment domain is outside the mesh domain, since it is periodic mesh segment domain will
        adjust accordingly.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
        mesh30 = Mesh(0.00, 2 * numpy.pi, True)
        mesh30.insert_mesh_segment(MeshSegment(4.5, 7, 0.01))

        if mesh30.is_valid():
            adj_mesh = mesh30.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_30_adj, adj_mesh)
            legacy_mesh = mesh30.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_30_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat",
                                        "output/output02_mesh_p_30.dat"))
        else:
            print("Phi test #30 mesh requirements are not valid.")

    def test_phi_31(self):
        """
        Periodic case where segment domain is outside the mesh domain, since it is periodic mesh segment domain will
        adjust accordingly.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
        mesh31 = Mesh(0.00, 2 * numpy.pi, True)
        mesh31.insert_mesh_segment(MeshSegment(-1.3, 0.2, 0.01))
        mesh31.insert_mesh_segment(MeshSegment(2, 4.2, 0.02))

        if mesh31.is_valid():
            adj_mesh = mesh31.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_31_adj, adj_mesh)
            legacy_mesh = mesh31.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_31_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat", "output/output02_mesh_p_31.dat"))
        else:
            print("Phi test #31 mesh requirements are not valid.")

    def test_phi_32(self):
        """
        Periodic case where segment domain is outside the mesh domain, since it is periodic mesh segment domain will
        adjust accordingly.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.07
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh32 = Mesh(0.00, 2 * numpy.pi, True)
        mesh32.insert_mesh_segment(MeshSegment(-1, 7, 0.015))

        if mesh32.is_valid():
            adj_mesh = mesh32.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_32_adj, adj_mesh)
            legacy_mesh = mesh32.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_32_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat", "output/output02_mesh_p_32.dat"))
        else:
            print("Phi test #32 mesh requirements are not valid.")

    def test_phi_33(self):
        """
        Periodic case where segment domain is outside the mesh domain, since it is periodic mesh segment domain will
        adjust accordingly.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.07
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh33 = Mesh(0.00, 2 * numpy.pi, True)
        mesh33.insert_mesh_segment(MeshSegment(-3, -2, 0.015))

        if mesh33.is_valid():
            adj_mesh = mesh33.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_33_adj, adj_mesh)
            legacy_mesh = mesh33.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_33_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat", "output/output02_mesh_p_33.dat"))
        else:
            print("Phi test #33 mesh requirements are not valid.")

    def test_phi_34(self):
        """
        Periodic case where segment domain is outside the mesh domain, since it is periodic mesh segment domain will
        adjust accordingly.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.07
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh34 = Mesh(0.00, 2 * numpy.pi, True)
        mesh34.insert_mesh_segment(MeshSegment(8, 9, 0.015))

        if mesh34.is_valid():
            adj_mesh = mesh34.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_34_adj, adj_mesh)
            legacy_mesh = mesh34.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_34_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat", "output/output02_mesh_p_34.dat"))
        else:
            print("Phi test #34 mesh requirements are not valid.")

    def test_phi_35(self):
        """
        Previously a bug Cooper found using the UI. Stuck in an infinite loop.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.04
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh35 = Mesh(0.00, 2 * numpy.pi, True)
        mesh35.insert_mesh_segment(MeshSegment(6.350741973844129, 6.600757014819878, 0.001, var_ds_ratio=1.03))
        mesh35.insert_mesh_segment(MeshSegment(6.233486857727595, 6.676342957440454, 0.0015, var_ds_ratio=1.02))
        mesh35.insert_mesh_segment(MeshSegment(5.457346587943244, 7.415520870668589, 0.02, var_ds_ratio=1.03))
        mesh35.insert_mesh_segment(MeshSegment(0.0, 6.28, 0.1, var_ds_ratio=1.05))
        mesh35.insert_mesh_segment(MeshSegment(3.939925780614721, 4.856704690278055, 0.044, var_ds_ratio=1.03))

        if mesh35.is_valid():
            adj_mesh = mesh35.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_35_adj, adj_mesh)
            legacy_mesh = mesh35.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_35_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat", "output/output02_mesh_p_35.dat"))
        else:
            print("Phi test #35 mesh requirements are not valid.")

    def test_phi_36(self):
        """
        Previously a bug Cooper found using the UI. - ratio was off in the end and begin segments since periodicity
         was not forced once the main legacy iteration was over.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.05
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh36 = Mesh(0.00, 2 * numpy.pi, True)
        mesh36.insert_mesh_segment(MeshSegment(5.102121, 5.330934, 0.004))
        mesh36.insert_mesh_segment(MeshSegment(5.084283, 5.356023, 0.002187))
        mesh36.insert_mesh_segment(MeshSegment(3.102121, 3.330934, 0.005))
        mesh36.insert_mesh_segment(MeshSegment(3.084283, 3.356023, 0.002))
        mesh36.insert_mesh_segment(MeshSegment(4.78382069, 5.65648531, 0.02))
        mesh36.insert_mesh_segment(MeshSegment(2.77669205, 7.66361395, 0.04))

        if mesh36.is_valid():
            adj_mesh = mesh36.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_36_adj, adj_mesh)
            legacy_mesh = mesh36.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_36_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat", "output/output02_mesh_p_36.dat"))
        else:
            print("Phi test #36 mesh requirements are not valid.")

    def test_phi_37(self):
        """
        The same test as phi_36 just that the last segment is lower. This previously caused an infinite loop.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.05
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh37 = Mesh(0.00, 2 * numpy.pi, True)
        mesh37.insert_mesh_segment(MeshSegment(5.102121, 5.330934, 0.004))
        mesh37.insert_mesh_segment(MeshSegment(5.084283, 5.356023, 0.002187))
        mesh37.insert_mesh_segment(MeshSegment(3.102121, 3.330934, 0.005))
        mesh37.insert_mesh_segment(MeshSegment(3.084283, 3.356023, 0.002))
        mesh37.insert_mesh_segment(MeshSegment(4.78382069, 5.65648531, 0.02))
        mesh37.insert_mesh_segment(MeshSegment(2.77669205, 7.66361395, 0.03))

        if mesh37.is_valid():
            adj_mesh = mesh37.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_37_adj, adj_mesh)
            legacy_mesh = mesh37.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_37_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat", "output/output02_mesh_p_37.dat"))
        else:
            print("Phi test #37 mesh requirements are not valid.")

    def test_phi_38(self):
        """
        This is a previous bug found by Cooper, more info:

        Test out the phi mesh input that caused the interface to crash in the current mesh_generator package.
        These numbers were created automatically based on a region and flux-rope selection in the interface.

        NOTES:
        - This particular region was near the left (phi=0) boundary.
        - It looks like the 3rd segment is causing the crash. The automatic box has a negative p0 value.
        - This sort of region *should* be handled by insert_mesh_segment for a periodic case
         (split up, limits corrected).
        - Perhaps the problem occurs earlier, but if I look at the output json after the "adjusted mesh" step,
          there is a segment with backwards s0 and s1 (set verbose_debug=True).
        - BUT for some reason, if I just add 2*pi to p0 and p1 for segment 3 it works fine (temporary_fix=True).
        - So, I'm guessing the problem may be at the segment correction stage (input sanitization).

        This issue was in insert_mesh_segment. - Solved.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.03
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh38 = Mesh(0.00, 2 * numpy.pi, True)
        mesh38.insert_mesh_segment(MeshSegment(0.186052, 0.415563, 0.004))
        mesh38.insert_mesh_segment(MeshSegment(0.217595, 0.387666, 0.002187))
        mesh38.insert_mesh_segment(MeshSegment(-0.13370181, 0.73896281, 0.02))
        mesh38.insert_mesh_segment(MeshSegment(4.14235485, 9.02927676, 0.04))

        if mesh38.is_valid():
            adj_mesh = mesh38.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 0.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2 * numpy.pi)
            self.assertEqual(adj_mesh["periodic"], True)
            self.assertEqual(phi_test_38_adj, adj_mesh)
            legacy_mesh = mesh38.build_legacy_mesh().json_dict()
            self.assertEqual(phi_test_38_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'p', "../mesh_generator/bin/", output_file_name="tmp_mesh_p.dat",
                            mesh_res_file_name="mesh_res_p.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_p.dat", "output/output02_mesh_p_38.dat"))
        else:
            print("Phi test #38 mesh requirements are not valid.")

    def test_r_1(self):
        """Results as expected. Test from real world example in Cooper's IDL code."""
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.03
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh1 = Mesh(1., 2.5, False)
        mesh1.insert_mesh_segment(MeshSegment(1, 1.009, 0.0003, var_ds_ratio=1.02))
        mesh1.insert_mesh_segment(MeshSegment(1.0090, 1.009800, 0.00050))
        mesh1.insert_mesh_segment(MeshSegment(1.009800, 1.017200, 0.00075))
        mesh1.insert_mesh_segment(MeshSegment(1.017200, 1.1000, 0.002))
        mesh1.insert_mesh_segment(MeshSegment(1.1000, 1.4000, 0.005))
        mesh1.insert_mesh_segment(MeshSegment(1.4000, 2, 0.02))

        if mesh1.is_valid():
            adj_mesh = mesh1.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 1.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2.5)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(r_test_1_adj, adj_mesh)
            legacy_mesh = mesh1.build_legacy_mesh().json_dict()
            self.assertEqual(r_test_1_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'r', "../mesh_generator/bin/", output_file_name="tmp_mesh_r.dat",
                            mesh_res_file_name="mesh_res_r.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_r.dat",
                                        "output/output02_mesh_r_1.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_r.dat",
                                        "py_mesh_res/mesh_res_r_1.dat"))
        else:
            print("Radial test #1 mesh requirements are not valid.")

    def test_r_2(self):
        """
        Results are as expected. Recalculate resolve gap when there is an undershoot case.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.09
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh2 = Mesh(1, 3, False)
        mesh2.insert_mesh_segment(MeshSegment(1, 1.02, var_ds_ratio=1.03))
        mesh2.insert_mesh_segment(MeshSegment(1.0, 1.2, 0.01, var_ds_ratio=1.03))
        mesh2.insert_mesh_segment(MeshSegment(1.0, 1.5, ds=0.02, var_ds_ratio=1.03))
        mesh2.insert_mesh_segment(MeshSegment(2.0, 2.5, ds=0.025, var_ds_ratio=1.06))

        if mesh2.is_valid():
            adj_mesh = mesh2.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 1.0)
            self.assertEqual(adj_mesh["upper_bnd"], 3.)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(r_test_2_adj, adj_mesh)
            legacy_mesh = mesh2.build_legacy_mesh().json_dict()
            self.assertEqual(r_test_2_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'r', "../mesh_generator/bin/", output_file_name="tmp_mesh_r.dat",
                            mesh_res_file_name="mesh_res_r.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_r.dat",
                                        "output/output02_mesh_r_2.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_r.dat",
                                        "py_mesh_res/mesh_res_r_2.dat"))
        else:
            print("Radial test #2 mesh requirements are not valid.")

    def test_r_3(self):
        """
        This test is developed by Cooper using the interactive UI. Saved as test_2020_04_27_CD2_radial_undershoot.json".
        Results are as expected. Resolve gap recalculate.
        """
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh3 = Mesh(1, 2.5, False)
        mesh3.insert_mesh_segment(MeshSegment(1, 1.02, 0.001, var_ds_ratio=1.03))
        mesh3.insert_mesh_segment(MeshSegment(1.0, 1.2, 0.01, var_ds_ratio=1.03))
        mesh3.insert_mesh_segment(MeshSegment(1.0, 1.5, ds=0.02, var_ds_ratio=1.03))
        mesh3.insert_mesh_segment(MeshSegment(2.0, 2.5, ds=0.025, var_ds_ratio=1.06))

        if mesh3.is_valid():
            adj_mesh = mesh3.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 1.0)
            self.assertEqual(adj_mesh["upper_bnd"], 2.5)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(r_test_3_adj, adj_mesh)
            legacy_mesh = mesh3.build_legacy_mesh().json_dict()
            self.assertEqual(r_test_3_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'r', "../mesh_generator/bin/", output_file_name="tmp_mesh_r.dat",
                            mesh_res_file_name="mesh_res_r.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_r.dat",
                                        "output/output02_mesh_r_3.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_r.dat",
                                        "py_mesh_res/mesh_res_r_3.dat"))
        else:
            print("Radial test #3 mesh requirements are not valid.")

    def test_r_4(self):
        """ Recalculate resolve gap. Results are as expected."""
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
        mesh4 = Mesh(1, 2.5, False)
        mesh4.insert_mesh_segment(MeshSegment(1.0, 1.2, 0.025, var_ds_ratio=1.06))
        mesh4.insert_mesh_segment(MeshSegment(1.0, 1.5, 0.02, var_ds_ratio=1.03))
        mesh4.insert_mesh_segment(MeshSegment(2.2, 2.3, ds=0.01, var_ds_ratio=1.03))
        mesh4.insert_mesh_segment(MeshSegment(2.3, 2.5, 0.001, var_ds_ratio=1.03))
        if mesh4.is_valid():
            adj_mesh = mesh4.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 1.)
            self.assertEqual(adj_mesh["upper_bnd"], 2.5)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(r_test_4_adj, adj_mesh)
            legacy_mesh = mesh4.build_legacy_mesh().json_dict()
            self.assertEqual(r_test_4_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'r', "../mesh_generator/bin/", output_file_name="tmp_mesh_r.dat",
                            mesh_res_file_name="mesh_res_r.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_r.dat",
                                        "output/output02_mesh_r_4.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_r.dat",
                                        "py_mesh_res/mesh_res_r_4.dat"))
        else:
            print("Radial test #4 mesh requirements are not valid.")

    def test_r_5(self):
        """ Create a mesh segment that does not have ds constraint only var_ds_ratio constraint.
        This example is taken from the thermo designer radial pot3d mesh."""
        MeshSegment.DEFAULT_BG_REGION_RATIO = 1.1
        MeshSegment.DEFAULT_FG_REGION_RATIO = 1.1
        mesh5 = Mesh(1, 10., False)
        mesh5.insert_mesh_segment(MeshSegment(s0=1., s1=1.05, ds=0.0001, var_ds_ratio=1.03))  # SEG 1
        mesh5.insert_mesh_segment(MeshSegment(s0=1.05, s1=1.3, ds=None, var_ds_ratio=1.03))  # SEG 2
        mesh5.insert_mesh_segment(MeshSegment(s0=1.3, s1=10., ds=None, var_ds_ratio=1.1))  # SEG 3
        if mesh5.is_valid():
            adj_mesh = mesh5.resolve_mesh_segments().json_dict()
            self.assertEqual(adj_mesh["lower_bnd"], 1.)
            self.assertEqual(adj_mesh["upper_bnd"], 10.)
            self.assertEqual(adj_mesh["periodic"], False)
            self.assertEqual(r_test_5_adj, adj_mesh)
            legacy_mesh = mesh5.build_legacy_mesh().json_dict()
            self.assertEqual(r_test_5_legacy, legacy_mesh)
            create_psi_mesh(adj_mesh, legacy_mesh, 'r', "../mesh_generator/bin/", output_file_name="tmp_mesh_r.dat",
                            mesh_res_file_name="mesh_res_r.dat", show_plot=False)
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/tmp_mesh_r.dat",
                                        "output/output02_mesh_r_5.dat"))
            self.assertTrue(filecmp.cmp("../mesh_generator/bin/mesh_res_r.dat",
                                        "py_mesh_res/mesh_res_r_5.dat"))
        else:
            print("Radial test #5 mesh requirements are not valid.")


if __name__ == '__main__':
    unittest.main()
