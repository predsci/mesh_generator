"""
 This is and example of how you would construct your mesh:

 1) mesh_requirements: (step 1) input mesh requirements. This includes: the mesh domain, periodicity (bool), BG_RATIO,
    FG_RATIO, and mesh segments.

 2) adjusted_mesh: (step 2) adjust mesh segments. After passing in the mesh requirements we call the
    function "resolve_mesh_segments()" in src/mesh.py. This function will:
        * delete small mesh segments (less than 4 points).
        * make the mesh continuous.
        * resolve mesh gaps.
        * solve for periodic case.

 3) legacy_mesh: (step 3) build legacy mesh: for each mesh segment solve the total number of points and ratio.
    Each legacy mesh segment is either constant, fixed_ds, or fixed_length. For more information
    read the documentation about legacy mesh calculations found in the mkdocs static site in the folder "my-mkdocs" or
    in the README.md.

 4) psi_mesh: (step 4) call_psi_mesh_tool.py to create an optimal mesh and write bin/mesh_res.dat and
    bin/output.dat file.The fortran mesh will check if the resulting legacy mesh fulfills the mesh requirements and will
    accordingly adjust the legacy mesh total number of points by shifting the mesh up or down.
    The resulting bin/output.dat file will be passed to MAS mesh.
"""

import numpy
from mesh_generator.src.mesh import Mesh
from mesh_generator.src.mesh_segment import MeshSegment
from mesh_generator.bin.call_psi_mesh_tool import create_psi_mesh


def theta_test_example():
    # Step 1 -> Input mesh requirements
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06  # default ratio in regions you do not care about. (Default is 1.06)
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03  # default ratio in regions you do care about. (Default is 1.03)
    mesh = Mesh(lower_bnd=0.00, upper_bnd=numpy.pi, periodic=False)  # mesh boundaries and if periodic

    # Mesh segment requirements:
    # s0 - segment begin, s1- segment end, ds- mesh spacing
    # var_ds_ratio- the maximum ratio between each point in the mesh segment. (Optional) by default is DEFAULT_FG_RATIO.
    mesh.insert_mesh_segment(MeshSegment(s0=1.10, s1=1.40, ds=0.01, var_ds_ratio=1.05))
    mesh.insert_mesh_segment(MeshSegment(s0=1.30, s1=1.90, ds=0.02))
    mesh.insert_mesh_segment(MeshSegment(s0=0.40, s1=2.80, ds=0.04, var_ds_ratio=1.02))

    # Step 2 -> Get adjusted mesh and save it as a dictionary. Later will be passed for the fortran call.
    adjusted_mesh = mesh.resolve_mesh_segments().json_dict()

    # Step 3 -> Get legacy mesh and save it as a dictionary. Later will be passed for the fortran call.
    legacy_mesh = mesh.build_legacy_mesh().json_dict()

    # Step 4 -> Call the fortran code.
    # get_plot = True (return a mesh spacing plot of mesh_res.txt) plotting is done by matplotlib.
    create_psi_mesh(adjusted_mesh, legacy_mesh, 't', "../mesh_generator/bin/", output_file_name="tmp_mesh_t.dat",
                    mesh_res_file_name="mesh_res_t.dat", show_plot=True)


if __name__ == "__main__":
    theta_test_example()
