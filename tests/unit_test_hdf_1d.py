""" This is a unit test file to write_1d_hdf_files.py"""

import unittest
from mesh_generator.hdf.read_mesh_res import read_tmp_file
from mesh_generator.hdf.psihdf import wrhdf_1d, rdhdf_1d
from scipy.interpolate import interp1d
import numpy as np


class TestMeshCases(unittest.TestCase):
    def test_1(self):
        """ read in the mesh files in idl_files folder then passing in the results in
        1d interpolating method and compare with hdf files on Q.
        """
        r, dr, r_ratio = read_tmp_file("/Users/opalissan/desktop/idl_files/mesh_res_r.dat")
        t, dt, t_ratio = read_tmp_file("/Users/opalissan/desktop/idl_files/mesh_res_t.dat")
        p, dp, p_ratio = read_tmp_file("/Users/opalissan/desktop/idl_files/mesh_res_p.dat")
        fac = 3  # example 1.

        # now lets calculate
        nR = len(r)
        nT = len(t)
        nP = len(p)
        nr_out = round(nR * fac)
        nt_out = round(nT * fac)
        np_out = round(nP * fac)

        # interpolate 1D.
        r_interp = interp1d(np.arange(nR), r, fill_value="extrapolate")
        t_interp = interp1d(np.arange(nT), t, fill_value="extrapolate")
        p_interp = interp1d(np.arange(nP), p, fill_value="extrapolate")

        r_new = r_interp(np.arange(nr_out) / (nr_out - 1) * (nR - 1))
        t_new = t_interp(np.arange(nt_out) / (nt_out - 1) * (nT - 1))
        p_new = p_interp(np.arange(np_out) / (np_out - 1) * (nP - 1))

        # read in true values for comparison. x is an empty array.
        x, r_true = rdhdf_1d('/Users/opalissan/desktop/mesh_folder/mesh_3x_main_r.hdf')
        x, t_true = rdhdf_1d('/Users/opalissan/desktop/mesh_folder/mesh_3x_main_t.hdf')
        x, p_true = rdhdf_1d('/Users/opalissan/desktop/mesh_folder/mesh_3x_main_p.hdf')

        # read half meshes saved on q.
        x, r_half_true = rdhdf_1d('/Users/opalissan/desktop/mesh_folder/mesh_3x_half_r.hdf')
        x, t_half_true = rdhdf_1d('/Users/opalissan/desktop/mesh_folder/mesh_3x_half_t.hdf')
        x, p_half_true = rdhdf_1d('/Users/opalissan/desktop/mesh_folder/mesh_3x_half_p.hdf')

        # check if the interpolation methods are the same.
        self.assertTrue(np.allclose(r_new, r_true, rtol=1e-6))
        self.assertTrue(np.allclose(t_new, t_true, rtol=1e-6))
        self.assertTrue(np.allclose(p_new, p_true, rtol=1e-6))

        # get the half mesh for this new main mesh.
        # initialize the half mesh arrays.
        r_half = np.zeros(nr_out + 1)
        t_half = np.zeros(nt_out + 1)
        p_half = np.zeros(np_out + 1)

        r_half[0] = r_new[0] - (r_new[1] - r_new[0]) / 2
        r_half[1: -1] = np.array([(np.roll(r_new, -1) + r_new)[: nr_out - 1] / 2])
        r_half[-1] = r_new[nr_out - 1] + (r_new[nr_out - 1] - r_new[nr_out - 2]) / 2

        t_half[0] = t_new[0] - (t_new[1] - t_new[0]) / 2
        t_half[1: -1] = np.array([(np.roll(t_new, -1) + t_new)[: nt_out - 1] / 2])
        t_half[-1] = t_new[nt_out - 1] + (t_new[nt_out - 1] - t_new[nt_out - 2]) / 2

        p_half[1: -1] = np.array([(np.roll(p_new, -1) + p_new)[: np_out - 1] / 2])
        p_half[0] = p_half[-2] - 2 * np.pi
        p_half[-1] = p_half[1] + 2 * np.pi

        # check if the interpolation methods are the same.
        self.assertTrue(np.allclose(r_half_true, r_half, rtol=1e-6))
        self.assertTrue(np.allclose(t_half_true, t_half, rtol=1e-6))
        self.assertTrue(np.allclose(p_half_true[1:], p_half[1:], rtol=1e-6))
        print(p_half_true[0] - p_half[0])


        # write hdf files.
        # wrhdf_1d(hdf_filename='/Users/opalissan/desktop/idl_files/mesh_2x_main_r.hdf', x=[], f=r_new)
        # wrhdf_1d(hdf_filename='/Users/opalissan/desktop/idl_files/mesh_2x_main_t.hdf', x=[], f=t_new)
        # wrhdf_1d(hdf_filename='/Users/opalissan/desktop/idl_files/mesh_2x_main_p.hdf', x=[], f=p_new)


if __name__ == '__main__':
    unittest.main()
