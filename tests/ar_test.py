"""
Here are all the tests found in test/unit_test.py. This is an easy way to check each step of creating a mesh.
"""
import numpy
from mesh_generator.src.mesh import Mesh
from mesh_generator.src.mesh_segment import MeshSegment


def get_mesh_theta_1():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(0.00, numpy.pi, False)
    mesh.insert_mesh_segment(MeshSegment(1.10, 1.40, 0.01))
    mesh.insert_mesh_segment(MeshSegment(1.30, 1.90, 0.02))
    mesh.insert_mesh_segment(MeshSegment(0.40, 2.80, 0.04))
    return mesh


def adjust__mesh_theta_1():
    mesh = get_mesh_theta_1()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_theta_1():
    mesh = adjust__mesh_theta_1()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_theta_2():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.1
    mesh = Mesh(0.00, numpy.pi)
    mesh.insert_mesh_segment(MeshSegment(1.367870, 1.405460, 0.000500))
    mesh.insert_mesh_segment(MeshSegment(1.369750, 1.406870, 0.000750))
    mesh.insert_mesh_segment(MeshSegment(1.323740, 1.438350, 0.002000))
    mesh.insert_mesh_segment(MeshSegment(1.305580, 1.797560, 0.005000))
    mesh.insert_mesh_segment(MeshSegment(1.268290, 1.910090, 0.010000))
    mesh.insert_mesh_segment(MeshSegment(0.873363, 2.123720, 0.020000))
    mesh.insert_mesh_segment(MeshSegment(1.225220, 1.445130, 0.015000))
    return mesh


def adjust__mesh_theta_2():
    mesh = get_mesh_theta_2()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_theta_2():
    mesh = adjust__mesh_theta_2()
    if mesh.is_valid():
        legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_theta_3():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(0.00, numpy.pi)
    mesh.insert_mesh_segment(MeshSegment(1, 3.1, 0.04))
    mesh.insert_mesh_segment(MeshSegment(2, 3, 0.01))
    mesh.insert_mesh_segment(MeshSegment(1.5, 2.5, 0.02))
    return mesh


def adjust__mesh_theta_3():
    mesh = get_mesh_theta_3()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_theta_3():
    mesh = adjust__mesh_theta_3()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_theta_4():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(0.00, numpy.pi, False)
    mesh.insert_mesh_segment(MeshSegment(1, 1.5, 0.033))
    mesh.insert_mesh_segment(MeshSegment(2, 2.5, 0.033))
    mesh.insert_mesh_segment(MeshSegment(0.5, 0.7, 0.02))
    return mesh


def adjust__mesh_theta_4():
    mesh = get_mesh_theta_4()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_theta_4():
    mesh = adjust__mesh_theta_4()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_theta_5():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(0.00, numpy.pi, False)
    mesh.insert_mesh_segment(MeshSegment(1, 1.5, 0.055))
    mesh.insert_mesh_segment(MeshSegment(2, 2.5, 0.035))
    return mesh


def adjust__mesh_theta_5():
    mesh = get_mesh_theta_5()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_theta_5():
    mesh = adjust__mesh_theta_5()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_theta_6():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.09
    mesh = Mesh(0.00, numpy.pi, False)
    mesh.insert_mesh_segment(MeshSegment(1, 1.5, 0.07, var_ds_ratio=1.09))
    mesh.insert_mesh_segment(MeshSegment(2, 2.5, 0.01))
    return mesh


def adjust__mesh_theta_6():
    mesh = get_mesh_theta_6()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_theta_6():
    mesh = adjust__mesh_theta_6()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_theta_7():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(0.00, numpy.pi, False)
    mesh.insert_mesh_segment(MeshSegment(1, 1.2, 0.02))
    mesh.insert_mesh_segment(MeshSegment(2, 2.5, 0.2))
    mesh.insert_mesh_segment(MeshSegment(1.5, 2.5, 0.03))
    mesh.insert_mesh_segment(MeshSegment(2.4, 3, 0.01))
    mesh.insert_mesh_segment(MeshSegment(0, 0.4, 0.03))
    mesh.insert_mesh_segment(MeshSegment(3, numpy.pi, 0.1))
    return mesh


def adjust__mesh_theta_7():
    mesh = get_mesh_theta_7()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_theta_7():
    mesh = adjust__mesh_theta_7()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_theta_8():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(0.00, numpy.pi, False)
    mesh.insert_mesh_segment(MeshSegment(0.5, 1.2, 0.2))
    mesh.insert_mesh_segment(MeshSegment(2, 3, 0.2))
    return mesh


def adjust__mesh_theta_8():
    mesh = get_mesh_theta_8()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_theta_8():
    mesh = adjust__mesh_theta_8()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_theta_9():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.1
    mesh = Mesh(0.00, numpy.pi, False)
    mesh.insert_mesh_segment(MeshSegment(1.1356989345533366, 1.5916809800055658, 0.01))
    mesh.insert_mesh_segment(MeshSegment(1.3532502198056797, 1.4283274775004602, 0.001))
    mesh.insert_mesh_segment(MeshSegment(0.8473725097896048, 2.06673071910115, 0.02))
    return mesh


def adjust__mesh_theta_9():
    mesh = get_mesh_theta_9()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_theta_9():
    mesh = adjust__mesh_theta_9()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_theta_10():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.1
    mesh = Mesh(0.00, numpy.pi, False)
    mesh.insert_mesh_segment(MeshSegment(0, numpy.pi, 0.05, var_ds_ratio=1.1))
    mesh.insert_mesh_segment(MeshSegment(1.3, 1.42, 0.001))
    return mesh


def adjust__mesh_theta_10():
    mesh = get_mesh_theta_10()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_theta_10():
    mesh = adjust__mesh_theta_10()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_theta_11():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(0.00, numpy.pi, False)
    mesh.insert_mesh_segment(MeshSegment(0, numpy.pi, 0.03))
    return mesh


def adjust__mesh_theta_11():
    mesh = get_mesh_theta_11()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_theta_11():
    mesh = adjust__mesh_theta_11()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_theta_12():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.02
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.04
    mesh = Mesh(0.00, numpy.pi, False)
    mesh.insert_mesh_segment(MeshSegment(0.3953275896083537, 0.7317766020409957, 0.01))
    mesh.insert_mesh_segment(MeshSegment(1.2364501206899585, 1.6906562874740247, 0.07))
    mesh.insert_mesh_segment(MeshSegment(1.8168246671362658, 2.61589107166379, 0.09))
    return mesh


def adjust__mesh_theta_12():
    mesh = get_mesh_theta_12()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_theta_12():
    mesh = adjust__mesh_theta_12()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_theta_13():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.09
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.04
    mesh = Mesh(0.00, numpy.pi, False)
    mesh.insert_mesh_segment(MeshSegment(0.4, 1.1, 0.07))
    mesh.insert_mesh_segment(MeshSegment(0.1, 0.4, 0.03))
    mesh.insert_mesh_segment(MeshSegment(1.1, 1.8, 0.03))
    mesh.insert_mesh_segment(MeshSegment(1.8, 2.3, 0.06))
    mesh.insert_mesh_segment(MeshSegment(2.3, 2.8, 0.02))
    return mesh


def adjust__mesh_theta_13():
    mesh = get_mesh_theta_13()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_theta_13():
    mesh = adjust__mesh_theta_13()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_theta_14():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.02
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.04
    mesh = Mesh(0.00, numpy.pi, False)
    mesh.insert_mesh_segment(MeshSegment(0.3953275896083537, 0.7317766020409957, 0.09))
    mesh.insert_mesh_segment(MeshSegment(1.2364501206899585, 1.6906562874740247, 0.07))
    mesh.insert_mesh_segment(MeshSegment(1.8168246671362658, 2.61589107166379, 0.01))
    return mesh


def adjust__mesh_theta_14():
    mesh = get_mesh_theta_14()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_theta_14():
    mesh = adjust__mesh_theta_14()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_theta_15():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.07
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
    mesh = Mesh(0.00, numpy.pi, False)
    mesh.insert_mesh_segment(MeshSegment(0., 0.73, 0.06))
    mesh.insert_mesh_segment(MeshSegment(1.2, 1.690, 0.001))
    return mesh


def adjust__mesh_theta_15():
    mesh = get_mesh_theta_15()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_theta_15():
    mesh = adjust__mesh_theta_15()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_theta_16():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.07
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
    mesh = Mesh(0.00, numpy.pi, False)
    mesh.insert_mesh_segment(MeshSegment(1.9, 2.3, 0.001))
    mesh.insert_mesh_segment(MeshSegment(2.5, numpy.pi, 0.07))
    return mesh


def adjust__mesh_theta_16():
    mesh = get_mesh_theta_16()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_theta_16():
    mesh = adjust__mesh_theta_16()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_theta_17():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.04
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
    mesh = Mesh(0.00, numpy.pi, False)
    mesh.insert_mesh_segment(MeshSegment(1.242461236299751, 1.3403353414879318, 0.001))
    mesh.insert_mesh_segment(MeshSegment(1.1164846652654585, 1.4469309015938716, 0.0015))
    mesh.insert_mesh_segment(MeshSegment(0.5193418749836787, 2.009257090100789, 0.02))
    mesh.insert_mesh_segment(MeshSegment(0.1, 3.04, 0.1))
    mesh.insert_mesh_segment(MeshSegment(1.465914082875949, 2.115869280025328, 0.047))
    return mesh


def adjust__mesh_theta_17():
    mesh = get_mesh_theta_17()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_theta_17():
    mesh = adjust__mesh_theta_17()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_1():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(3.0, 3.3, 0.01))
    mesh.insert_mesh_segment(MeshSegment(2.8, 3.8, 0.02))
    mesh.insert_mesh_segment(MeshSegment(0.4, 2 * numpy.pi - 0.3, 0.04))
    return mesh


def adjust__mesh_phi_1():
    mesh = get_mesh_phi_1()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_1():
    mesh = adjust__mesh_phi_1()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_2():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0.0, 3.3, 0.01))
    mesh.insert_mesh_segment(MeshSegment(2.8, 3.8, 0.02))
    mesh.insert_mesh_segment(MeshSegment(3, 4.5, 0.04))
    return mesh


def adjust__mesh_phi_2():
    mesh = get_mesh_phi_2()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_2():
    mesh = adjust__mesh_phi_2()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_3():
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(1, 5.5, 0.02))
    mesh.insert_mesh_segment(MeshSegment(2.2, 3, 0.03))
    mesh.insert_mesh_segment(MeshSegment(3.01, 4.5, 0.05))
    return mesh


def adjust__mesh_phi_3():
    mesh = get_mesh_phi_3()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_3():
    mesh = adjust__mesh_phi_3()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_4():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(3.01, 3.03, 0.02))
    mesh.insert_mesh_segment(MeshSegment(3.01, 4.5, 0.05))
    mesh.insert_mesh_segment(MeshSegment(4, 6.25, 0.01))
    return mesh


def adjust__mesh_phi_4():
    mesh = get_mesh_phi_4()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_4():
    mesh = adjust__mesh_phi_4()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_5():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0.5, 1.1, 0.09))
    mesh.insert_mesh_segment(MeshSegment(0.7, 1.3, 0.07))
    return mesh


def adjust__mesh_phi_5():
    mesh = get_mesh_phi_5()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_5():
    mesh = adjust__mesh_phi_5()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_6():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0, 1.1, 0.03))
    mesh.insert_mesh_segment(MeshSegment(2, 3, 0.07))
    mesh.insert_mesh_segment(MeshSegment(2, 6.1, 0.05))
    mesh.insert_mesh_segment(MeshSegment(6, 2 * numpy.pi, 0.01))
    return mesh


def adjust__mesh_phi_6():
    mesh = get_mesh_phi_6()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_6():
    mesh = adjust__mesh_phi_6()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_7():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(1, 1.8, 0.03))
    return mesh


def adjust__mesh_phi_7():
    mesh = get_mesh_phi_7()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_7():
    mesh = adjust__mesh_phi_7()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_8():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.08, var_ds_ratio=1.06))
    mesh.insert_mesh_segment(MeshSegment(1, 1.8, 0.01))
    mesh.insert_mesh_segment(MeshSegment(1.4, 1.8, 0.001))
    mesh.insert_mesh_segment(MeshSegment(0.0, 3.8, 0.02))
    return mesh


def adjust__mesh_phi_8():
    mesh = get_mesh_phi_8()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_8():
    mesh = adjust__mesh_phi_8()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_9():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.1, var_ds_ratio=1.06))
    mesh.insert_mesh_segment(MeshSegment(2.4, 4.8, 0.001))
    mesh.insert_mesh_segment(MeshSegment(4.7, 2 * numpy.pi, 0.01))
    return mesh


def adjust__mesh_phi_9():
    mesh = get_mesh_phi_9()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_9():
    mesh = adjust__mesh_phi_9()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_10():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.06, var_ds_ratio=1.06))
    mesh.insert_mesh_segment(MeshSegment(2.4, 4.8, 0.001))
    mesh.insert_mesh_segment(MeshSegment(4.7, 2 * numpy.pi, 0.01))
    return mesh


def adjust__mesh_phi_10():
    mesh = get_mesh_phi_10()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_10():
    mesh = adjust__mesh_phi_10()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_11():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.05
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.07, var_ds_ratio=1.04))
    mesh.insert_mesh_segment(MeshSegment(2.4, 4.8, 0.002))
    mesh.insert_mesh_segment(MeshSegment(3.4, 4.1, 0.001))
    mesh.insert_mesh_segment(MeshSegment(4.7, 5.5, 0.01))
    mesh.insert_mesh_segment(MeshSegment(2, 3, 0.01))
    return mesh


def adjust__mesh_phi_11():
    mesh = get_mesh_phi_11()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_11():
    mesh = adjust__mesh_phi_11()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_12():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.05
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.05, var_ds_ratio=1.04))
    mesh.insert_mesh_segment(MeshSegment(3.4, 4.1, 0.001))
    mesh.insert_mesh_segment(MeshSegment(4.7, 5.5, 0.01))
    mesh.insert_mesh_segment(MeshSegment(2, 3, 0.01))
    return mesh


def adjust__mesh_phi_12():
    mesh = get_mesh_phi_12()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_12():
    mesh = adjust__mesh_phi_12()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_13():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.05
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.04, var_ds_ratio=1.04))
    mesh.insert_mesh_segment(MeshSegment(3.4, 4.1, 0.03))
    mesh.insert_mesh_segment(MeshSegment(4.7, 5.5, 0.01))
    mesh.insert_mesh_segment(MeshSegment(2, 3, 0.01))
    return mesh


def adjust__mesh_phi_13():
    mesh = get_mesh_phi_13()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_13():
    mesh = adjust__mesh_phi_13()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_14():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(1.4130858522170961, 2.1280400036364604, 0.03))
    mesh.insert_mesh_segment(MeshSegment(0.9504684601222136, 2.237385932677069, 0.05))
    mesh.insert_mesh_segment(MeshSegment(1.514020555946889, 1.7327124140281063, 0.001))
    return mesh


def adjust__mesh_phi_14():
    mesh = get_mesh_phi_14()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_14():
    mesh = adjust__mesh_phi_14()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_15():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.09
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.09, var_ds_ratio=1.06))
    mesh.insert_mesh_segment(MeshSegment(3.4, 4.1, 0.03))
    mesh.insert_mesh_segment(MeshSegment(2, 2.2, 0.001))
    mesh.insert_mesh_segment(MeshSegment(0, 3, 0.01))
    return mesh


def adjust__mesh_phi_15():
    mesh = get_mesh_phi_15()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_15():
    mesh = adjust__mesh_phi_15()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_16():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.09
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.01))
    return mesh


def adjust__mesh_phi_16():
    mesh = get_mesh_phi_16()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_16():
    mesh = adjust__mesh_phi_16()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_17():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.04
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.03))
    mesh.insert_mesh_segment(MeshSegment(4.886921905584123, 5.467296452030431, 0.01))
    mesh.insert_mesh_segment(MeshSegment(2.052338975839116, 3.3308452230831547, 0.02))
    return mesh


def adjust__mesh_phi_17():
    mesh = get_mesh_phi_17()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_17():
    mesh = adjust__mesh_phi_17()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_18():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.04
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.02))
    mesh.insert_mesh_segment(MeshSegment(3.3476676737047866, 3.6925279114482451, 0.001))
    mesh.insert_mesh_segment(MeshSegment(2.472900241379918, 3.843929967042934, 0.01))
    return mesh


def adjust__mesh_phi_18():
    mesh = get_mesh_phi_18()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_18():
    mesh = adjust__mesh_phi_18()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_19():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.05
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(3.3476676737047866, 3.6925279114482451, 0.001))
    mesh.insert_mesh_segment(MeshSegment(2.472900241379918, 3.843929967042934, 0.04))
    mesh.insert_mesh_segment(MeshSegment(4, 4.5, 0.04))
    return mesh


def adjust__mesh_phi_19():
    mesh = get_mesh_phi_19()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_19():
    mesh = adjust__mesh_phi_19()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_20():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.02
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.04
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(1.3037399231764875, 1.8168246671362667, 0.01))
    mesh.insert_mesh_segment(MeshSegment(1.2532725713115913, 1.8672920190011628, 0.07))
    mesh.insert_mesh_segment(MeshSegment(2.0186940745958517, 2.7252370007043996, 0.09))
    return mesh


def adjust__mesh_phi_20():
    mesh = get_mesh_phi_20()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_20():
    mesh = adjust__mesh_phi_20()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_21():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.02
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.04
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(1.3037399231764875, 1.8168246671362667, 0.09))
    mesh.insert_mesh_segment(MeshSegment(1.2532725713115913, 1.8672920190011628, 0.07))
    mesh.insert_mesh_segment(MeshSegment(2.0186940745958517, 2.7252370007043996, 0.01))
    return mesh


def adjust__mesh_phi_21():
    mesh = get_mesh_phi_21()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_21():
    mesh = adjust__mesh_phi_21()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_22():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.07
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.04
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(1.3037399231764875, 1.8168246671362667, 0.09))
    mesh.insert_mesh_segment(MeshSegment(2.0186940745958517, 2.7252370007043996, 0.01))
    mesh.insert_mesh_segment(MeshSegment(1.2532725713115913, 3.8672920190011628, 0.07))
    return mesh


def adjust__mesh_phi_22():
    mesh = get_mesh_phi_22()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_22():
    mesh = adjust__mesh_phi_22()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_23():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0.3953275896083546, 1.3626185003522, 0.06))
    mesh.insert_mesh_segment(MeshSegment(2.262619608609517, 2.8934615069207203, 0.01))
    mesh.insert_mesh_segment(MeshSegment(3.3644901243264194, 4.163556528853944, 0.001))
    mesh.insert_mesh_segment(MeshSegment(0.44579494147325105, 3.986920797326807, 0.08))
    mesh.insert_mesh_segment(MeshSegment(2.5570124944880788, 2.7168257753935836, 0.001))
    return mesh


def adjust__mesh_phi_23():
    mesh = get_mesh_phi_23()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_23():
    mesh = adjust__mesh_phi_23()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_24():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0.3953275896083546, 1.3626185003522, 0.06))
    mesh.insert_mesh_segment(MeshSegment(2.262619608609517, 2.8934615069207203, 0.01))
    mesh.insert_mesh_segment(MeshSegment(3.3644901243264194, 4.163556528853944, 0.001))
    mesh.insert_mesh_segment(MeshSegment(0.44579494147325105, 3.986920797326807, 0.08))
    mesh.insert_mesh_segment(MeshSegment(2.5570124944880788, 2.7168257753935836, 0.001))
    mesh.insert_mesh_segment(MeshSegment(0, 2 * numpy.pi, 0.06))
    return mesh


def adjust__mesh_phi_24():
    mesh = get_mesh_phi_24()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_24():
    mesh = adjust__mesh_phi_24()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_25():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0.3953275896083546, 1.3626185003522, 0.06))
    mesh.insert_mesh_segment(MeshSegment(2.262619608609517, 2.8934615069207203, 0.01))
    mesh.insert_mesh_segment(MeshSegment(3.3644901243264194, 4.163556528853944, 0.001))
    mesh.insert_mesh_segment(MeshSegment(0.44579494147325105, 3.986920797326807, 0.08))
    mesh.insert_mesh_segment(MeshSegment(2.5570124944880788, 2.7168257753935836, 0.001))
    mesh.insert_mesh_segment(MeshSegment(0, 2 * numpy.pi, 0.07))
    return mesh


def adjust__mesh_phi_25():
    mesh = get_mesh_phi_25()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_25():
    mesh = adjust__mesh_phi_25()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_26():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0.3953275896083546, 1.3626185003522, 0.001))
    mesh.insert_mesh_segment(MeshSegment(2.262619608609517, 2.8934615069207203, 0.01))
    mesh.insert_mesh_segment(MeshSegment(3.3644901243264194, 4.163556528853944, 0.06))
    mesh.insert_mesh_segment(MeshSegment(0.44579494147325105, 3.986920797326807, 0.08))
    mesh.insert_mesh_segment(MeshSegment(2.5570124944880788, 2.7168257753935836, 0.001))
    mesh.insert_mesh_segment(MeshSegment(0, 2 * numpy.pi, 0.07))
    return mesh


def adjust__mesh_phi_26():
    mesh = get_mesh_phi_26()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_26():
    mesh = adjust__mesh_phi_26()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_27():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0.3953275896083546, 1.3626185003522, 0.001))
    mesh.insert_mesh_segment(MeshSegment(3.3644901243264194, 4.163556528853944, 0.06))
    mesh.insert_mesh_segment(MeshSegment(3.3644901243264194, 5.163556528853944, 0.09))
    return mesh


def adjust__mesh_phi_27():
    mesh = get_mesh_phi_27()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_27():
    mesh = adjust__mesh_phi_27()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_28():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.04
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0.753275896083546, 1.3626185003522, 0.12))
    mesh.insert_mesh_segment(MeshSegment(4.1644901243264194, 4.363556528853944, 0.0001))
    return mesh


def adjust__mesh_phi_28():
    mesh = get_mesh_phi_28()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_28():
    mesh = adjust__mesh_phi_28()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_29():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0., 2 * numpy.pi, 0.03))
    mesh.insert_mesh_segment(MeshSegment(4.886921905584123, 5.467296452030431, 0.01))
    mesh.insert_mesh_segment(MeshSegment(3.886921905584123, 4.467296452030431, 0.0001))
    mesh.insert_mesh_segment(MeshSegment(3.052338975839116, 3.3308452230831547, 0.02))
    return mesh


def adjust__mesh_phi_29():
    mesh = get_mesh_phi_29()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_29():
    mesh = adjust__mesh_phi_29()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_30():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(4.5, 7, 0.01))
    return mesh


def adjust__mesh_phi_30():
    mesh = get_mesh_phi_30()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_30():
    mesh = adjust__mesh_phi_30()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_31():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(-1.3, 0.2, 0.01))
    mesh.insert_mesh_segment(MeshSegment(2, 4.2, 0.02))
    return mesh


def adjust__mesh_phi_31():
    mesh = get_mesh_phi_31()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_31():
    mesh = adjust__mesh_phi_31()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_32():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.07
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(-1, 7, 0.015))
    return mesh


def adjust__mesh_phi_32():
    mesh = get_mesh_phi_32()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_32():
    mesh = adjust__mesh_phi_32()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_33():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.07
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(-3, -2, 0.015))
    return mesh


def adjust__mesh_phi_33():
    mesh = get_mesh_phi_33()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_33():
    mesh = adjust__mesh_phi_33()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_34():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.07
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(8, 9, 0.015))
    return mesh


def adjust__mesh_phi_34():
    mesh = get_mesh_phi_34()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_34():
    mesh = adjust__mesh_phi_34()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_35():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.04
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(6.350741973844129, 6.600757014819878, 0.001, var_ds_ratio=1.03))
    mesh.insert_mesh_segment(MeshSegment(6.233486857727595, 6.676342957440454, 0.0015, var_ds_ratio=1.02))
    mesh.insert_mesh_segment(MeshSegment(5.457346587943244, 7.415520870668589, 0.02, var_ds_ratio=1.03))
    mesh.insert_mesh_segment(MeshSegment(0.0, 6.28, 0.1, var_ds_ratio=1.05))
    mesh.insert_mesh_segment(MeshSegment(3.939925780614721, 4.856704690278055, 0.044, var_ds_ratio=1.03))
    return mesh


def adjust__mesh_phi_35():
    mesh = get_mesh_phi_35()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_35():
    mesh = adjust__mesh_phi_35()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_36():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.05
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(5.102121, 5.330934, 0.004))
    mesh.insert_mesh_segment(MeshSegment(5.084283, 5.356023, 0.002187))
    mesh.insert_mesh_segment(MeshSegment(3.102121, 3.330934, 0.005))
    mesh.insert_mesh_segment(MeshSegment(3.084283, 3.356023, 0.002))
    mesh.insert_mesh_segment(MeshSegment(4.78382069, 5.65648531, 0.02))
    mesh.insert_mesh_segment(MeshSegment(2.77669205, 7.66361395, 0.04))
    return mesh


def adjust__mesh_phi_36():
    mesh = get_mesh_phi_36()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_36():
    mesh = adjust__mesh_phi_36()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_37():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.05
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(5.102121, 5.330934, 0.004))
    mesh.insert_mesh_segment(MeshSegment(5.084283, 5.356023, 0.002187))
    mesh.insert_mesh_segment(MeshSegment(3.102121, 3.330934, 0.005))
    mesh.insert_mesh_segment(MeshSegment(3.084283, 3.356023, 0.002))
    mesh.insert_mesh_segment(MeshSegment(4.78382069, 5.65648531, 0.02))
    mesh.insert_mesh_segment(MeshSegment(2.77669205, 7.66361395, 0.03))
    return mesh


def adjust__mesh_phi_37():
    mesh = get_mesh_phi_37()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_37():
    mesh = adjust__mesh_phi_37()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_phi_38():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.03
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(0.186052, 0.415563, 0.004))
    mesh.insert_mesh_segment(MeshSegment(0.217595, 0.387666, 0.002187))
    mesh.insert_mesh_segment(MeshSegment(-0.13370181, 0.73896281, 0.02))
    mesh.insert_mesh_segment(MeshSegment(4.14235485, 9.02927676, 0.04))

    return mesh


def adjust__mesh_phi_38():
    mesh = get_mesh_phi_38()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_phi_38():
    mesh = adjust__mesh_phi_38()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


'''
RADIUS 1- TEST CASE 
  ; Radial mesh (manual)
  Ds2 = max([Ds0,0.03])
  Locations = [1.0000,  1.0090,  1.009800, 1.017200, 1.1000,  1.4000, 2.0000,  rMAx]
  Deltas =    [     0.0003,  0.00050,  0.00075,   0.002,   0.005,  0.02,   -7777.]
  Ratios =    [     1.0200,  RatR,     RatR,      RatR,    RatR,   RatR,   RatR]

'''


def get_mesh_r_1():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.03
    mesh = Mesh(1., 2.5, False)
    mesh.insert_mesh_segment(MeshSegment(1, 1.009, 0.0003, var_ds_ratio=1.02))
    mesh.insert_mesh_segment(MeshSegment(1.0090, 1.009800, 0.00050))
    mesh.insert_mesh_segment(MeshSegment(1.009800, 1.017200, 0.00075))
    mesh.insert_mesh_segment(MeshSegment(1.017200, 1.1000, 0.002))
    mesh.insert_mesh_segment(MeshSegment(1.1000, 1.4000, 0.005))
    mesh.insert_mesh_segment(MeshSegment(1.4000, 2, 0.02))

    return mesh


def adjust__mesh_r_1():
    mesh = get_mesh_r_1()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_r_1():
    mesh = adjust__mesh_r_1()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


'''
RADIUS 2- 02_custom_mesh_thermo_and_zbpfss.txt
  ; Radial mesh (manual)
RatGood=1.03
RatR = 1.03
RatR2 = 1.016 
rMaxThermo = 30.0
rMaxPFSS = 2.5

'''


def get_mesh_r_2():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.09
    mesh = Mesh(1, 3, False)
    mesh.insert_mesh_segment(MeshSegment(1, 1.02, var_ds_ratio=1.03))
    mesh.insert_mesh_segment(MeshSegment(1.0, 1.2, 0.01, var_ds_ratio=1.03))
    mesh.insert_mesh_segment(MeshSegment(1.0, 1.5, ds=0.02, var_ds_ratio=1.03))
    mesh.insert_mesh_segment(MeshSegment(2.0, 2.5, ds=0.025, var_ds_ratio=1.06))
    # mesh.insert_mesh_segment(MeshSegment(2.5, 2.9, ds=0.025, var_ds_ratio=None))
    return mesh


def adjust__mesh_r_2():
    mesh = get_mesh_r_2()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_r_2():
    mesh = adjust__mesh_r_2()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_r_3():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(1, 2.5, False)
    mesh.insert_mesh_segment(MeshSegment(1, 1.02, 0.001, var_ds_ratio=1.03))
    mesh.insert_mesh_segment(MeshSegment(1.0, 1.2, 0.01, var_ds_ratio=1.03))
    mesh.insert_mesh_segment(MeshSegment(1.0, 1.5, ds=0.02, var_ds_ratio=1.03))
    mesh.insert_mesh_segment(MeshSegment(2.0, 2.5, ds=0.025, var_ds_ratio=1.06))
    return mesh


def adjust__mesh_r_3():
    mesh = get_mesh_r_3()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_r_3():
    mesh = adjust__mesh_r_3()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_r_4():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(1, 2.5, False)
    mesh.insert_mesh_segment(MeshSegment(1.0, 1.2, 0.025, var_ds_ratio=1.06))
    mesh.insert_mesh_segment(MeshSegment(1.0, 1.5, 0.02, var_ds_ratio=1.03))
    mesh.insert_mesh_segment(MeshSegment(2.2, 2.3, ds=0.01, var_ds_ratio=1.03))
    mesh.insert_mesh_segment(MeshSegment(2.3, 2.5, 0.001, var_ds_ratio=1.03))
    return mesh


def adjust__mesh_r_4():
    mesh = get_mesh_r_4()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_r_4():
    mesh = adjust__mesh_r_4()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_r_5():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.1
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.1
    # STEP 1.
    # rMax is the upper bound of radial mesh.
    rMax = 10.

    # mesh boundaries and if periodic.
    mesh = Mesh(lower_bnd=1., upper_bnd=rMax, periodic=False)

    # Mesh segment requirements:
    # s0 - segment begin, s1- segment end, ds- mesh spacing
    # (Optional) var_ds_ratio- the maximum ratio between each point in the mesh segment.
    mesh.insert_mesh_segment(MeshSegment(s0=1., s1=1.05, ds=0.0001, var_ds_ratio=1.03))  # SEG 1
    mesh.insert_mesh_segment(MeshSegment(s0=1.05, s1=1.3, ds=None, var_ds_ratio=1.03))  # SEG 2
    mesh.insert_mesh_segment(MeshSegment(s0=1.3, s1=rMax, ds=None, var_ds_ratio=1.1))  # SEG 3
    return mesh


def adjust__mesh_r_5():
    mesh = get_mesh_r_5()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_r_5():
    mesh = adjust__mesh_r_5()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


def get_mesh_r_6():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.1
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03
    # rMax is the upper bound of radial mesh.
    rMax = 10.
    # mesh boundaries and if periodic.
    mesh = Mesh(lower_bnd=1., upper_bnd=rMax, periodic=False)
    mesh.insert_mesh_segment(MeshSegment(s0=1.3, s1=rMax + 1, ds=0.021, var_ds_ratio=1.1))  # SEG 3
    return mesh


def adjust__mesh_r_6():
    mesh = get_mesh_r_6()
    if mesh.is_valid():
        mesh.resolve_mesh_segments()
    return mesh


def legacy__mesh_r_6():
    mesh = adjust__mesh_r_6()
    legacy_mesh = mesh.build_legacy_mesh()
    return legacy_mesh


if __name__ == "__main__":
    legacy__mesh_theta_2()
    print(legacy__mesh_theta_2())
