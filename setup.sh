#!/bin/bash
# -----------------------------------------------------------------------------
# Script     : setup.sh.
# Description: Sets up Python virtual environment.
# -----------------------------------------------------------------------------

export INCLUDE_DIRS="${PS_EXT_DEPS_HOME}/hdf4/include"
export LIBRARY_DIRS="${PS_EXT_DEPS_HOME}/hdf4/lib"
export LIBRARY_DIRS="${LIBRARY_DIRS}:${PS_EXT_DEPS_HOME}/jpeg/lib"
export LIBRARY_DIRS="${LIBRARY_DIRS}:${PS_EXT_DEPS_HOME}/zlib/lib"

# -----------------------------------------------------------------------------

declare -ir STATUS_OK=0
declare -ir STATUS_ERROR=1

declare -ir NUM_REQUIRED_ARGS=0

function print_usage_msg {
  printf "
 Script     : setup.sh.
 Description: Sets up Python virtual environment.

 Usage: setup.sh
 \n"
}

# Sets up Python virtual environment.
function setup_python_venv_directory {
  [[ -d venv ]] && rm -rf venv

  mkdir venv
  [[ $? -ne ${STATUS_OK} ]] && return ${STATUS_ERROR}

  pushd venv > /dev/null
    python3 -m venv "$(pwd)"
    [[ $? -ne ${STATUS_OK} ]] && return ${STATUS_ERROR}

    source bin/activate
    [[ $? -ne ${STATUS_OK} ]] && return ${STATUS_ERROR}

    while read requirement; do
      pip3 install "${requirement}"
      [[ $? -ne ${STATUS_OK} ]] && return ${STATUS_ERROR}
    done < ../requirements.txt

    deactivate
    [[ $? -ne ${STATUS_OK} ]] && return ${STATUS_ERROR}
  popd > /dev/null

  return ${STATUS_OK}
}

function main {
  setup_python_venv_directory
  if [[ $? -ne ${STATUS_OK} ]]; then
    printf "ERROR: Failed setting up Python virtual environment.\n" >&2
    return ${STATUS_ERROR}
  fi

  printf "\n"
  printf "Python virtual environment setup is complete!\n"
  printf "\n"
  return ${STATUS_OK}
}

#------------------------------------------------------------------------------

if [[ "$1" = "-h" || "$1" = "-help" || "$1" = "--help" ]]; then
  print_usage_msg
  exit ${STATUS_OK}
fi

if [[ $# -ne ${NUM_REQUIRED_ARGS} ]]; then
  print_usage_msg
  exit ${STATUS_ERROR}
fi

main "$@"
exit $?
