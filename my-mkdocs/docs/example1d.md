
# Example 1D Mesh
   `/test/create_mesh.py`
   - This an example for creating a 1D mesh. Including 4 steps of creating a mesh.
    
- Read the comments inside the example python scripts for more details.

## Edit the python script: `/test/create_mesh.py`
- **Import the following**:

```python 
import numpy
from mesh_generator.src.mesh import Mesh
from mesh_generator.src.mesh_segment import MeshSegment
from mesh_generator.bin.call_psi_mesh_tool import create_psi_mesh
```  

- **Step 1** : Input mesh requirements. Make sure to specify:
    
    Mesh:
    
      - Set `lower_bnd` and `upper_bnd` limits of mesh.
      
      - Set `periodic`.
      
      - Set `DEFAULT_BG_REGION_RATIO` - Ratio in areas without ds constraint. (Optional)
      
      - Set `DEFAULT_FG_REGION_RATIO` - Ratio in areas with ds constraint. (Optional)
      
    Mesh segment:
    
      - Set `s0` and `s1` for segment domain limits.
      
      - Set `ds` to the resolution you want.
      
      - Set `var_ds_ratio` as segment maximum cell to cell mesh expansion ratio. (Optional)

```python 
# default ratio in regions you do not care about. (Default is 1.06)
MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06 

# default ratio in regions you do care about. (Default is 1.03) 
MeshSegment.DEFAULT_FG_REGION_RATIO = 1.03  

# mesh boundaries and if periodic
mesh = Mesh(lower_bnd=0.00, upper_bnd=numpy.pi, periodic=False) 

# Mesh segment requirements:
# s0 - segment begin, s1- segment end, ds- mesh spacing
# (Optional) var_ds_ratio- the maximum ratio between each point in the mesh segment. 
mesh.insert_mesh_segment(MeshSegment(s0=1.10, s1=1.40, ds=0.01, var_ds_ratio=1.05))
mesh.insert_mesh_segment(MeshSegment(s0=1.30, s1=1.90, ds=0.02))
mesh.insert_mesh_segment(MeshSegment(s0=0.40, s1=2.80, ds=0.04, var_ds_ratio=1.02))

input_mesh = mesh.json_dict()
```  

- **Step 2**: Get adjusted mesh and save it as a dictionary. Later will be passed for the fortran call.       
        
```python 
adjusted_mesh = mesh.resolve_mesh_segments().json_dict()
```

- **Step 3**: Get legacy mesh and save it as a dictionary. Later will be passed for the fortran call.      
        
```python 
legacy_mesh = mesh.build_legacy_mesh().json_dict()
```   

- **Step 4**: Call the fortran code.
```python 
# show_plot = True (return a mesh spacing plot of mesh_res.txt) plotting is done by matplotlib.
# save_plot = True (saves the plot as a png in "plots" folder.
create_psi_mesh(adjusted_mesh, legacy_mesh, mesh_type="t", dir_name=os.getcwd(), output_file_name="tmp_mesh_t.dat", 
        mesh_res_file_name="mesh_res_t.dat",save_plot=True, show_plot=False, save_plot_path=os.getcwd(),
        plot_file_name="t_mesh_spacing.png", input_mesh=input_mesh)
```
- **Step 5**: Call the fortran code. Inspect the results:

    - mesh_generator/bin/mesh_res_[t/p/r].txt - file with mesh points. 
    
    - mesh_generator/bin/tmp_mesh_[t/p/r].dat - file with legacy mesh results. 
    
    - Setting get_plot=True will return a plot of mesh_res_[t/p/r].txt. The plot below is an example.     

![Screenshot](images/create_mesh_ex.png)


