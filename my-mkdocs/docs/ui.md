#  Mesh UI (User Interface)

The tkinter user interface is an easy way to create a 3D Mesh. 
To start the Mesh generator UI:

```python
from mesh_generator import MeshGeneratorUI
MeshGeneratorUI()
```

Then, if you desire to skip the interactive main window then supply:

- Save mesh file directory 
- Previous session json file. 

To open the main window UI, supply:

- Save mesh file directory.
- hdf/h5 magnetogram file.
- Previous session json file. (Optional)

**First window:**
![](images/firstwindow_ui_screenshot.png)


Find the correct path to:

- hdf/h5 magnetogram file which will be plotted in the main window (next page). 
- Previous session json file if you wish to restore the mesh requirements. 
- Folder to save the mesh results (tmp files, plots, 1d hdf/h5 files, mesh header file).


**The mainapp window**:
![](images/ui_main_app.png)
### **Step 1**
Draw rectangle patches on plot, when patch is finalized click `Save latest box`. There is an option to 
delete, redraw and restart drawing the rectangular patches. If you prefer to enter the patches coordinates manually 
then click `Box Chart`.

![](images/ui_gif.gif)

![](images/step1_zoom_shot.png)

### **Step 2**
Input the resolution of each box (`ds`). `var_ds_ratio` is optional. 

(Optional) Input `BG_RATIO` and `FG_RATIO`. Default `BG_RATIO` is 1.06, and default `FG_RATIO` is 1.03. 

**`BG_RATIO`**- ratio in areas that do **not** have specified ds. 

**`FG_RATIO`** - ratio in patches (areas that have a specified ds).

![](images/step2_zoom_shot.png)


### **Step 3**
Get theta and phi mesh results. First column will print on screen the total number of points in 
each mesh, and the second column will open a matplotlib window with `mesh_res.txt` plot results. 


![](images/step4_zoom_shot.png)


### **Step 4**
Manually create the radial mesh. 

 - Input `RADIAL_BG_RATIO` and `RADIAL_FG_RATIO` similarly to **Step 3**. 

 - Input the Radial interval: `rMin` and `rMax`. 

 - In the table indicate the mesh segment requirements. Again, here the `var_ds_ratio` is optional. 

 - Then, get mesh results by clicking the buttons `r number of points` and `r mesh results`.


![](images/step5_zoom_shot.png)

### **Step 5**
 When done, click `Save Session`. The button is located under the magnetogram plot and will save the mesh dictionary 
 as a txt/json file in the folder provided in the first window. To access this tutorials dictionary go to 
  `ui_session_files/tutorial.json`.

In addition, in the save folder there will be:

- tmp mesh files with mesh spacing, resolution, and cell-to-cell ratio.
- tmp mas files. 
- plots.
- 1D main and half mesh hdf files.

```python
{
    "lower_bnd_t": 0.0,
    "upper_bnd_t": 3.141592653589793,
    "lower_bnd_p": 0.0,
    "upper_bnd_p": 6.283185307179586,
    "lower_bnd_r": 1.0,
    "upper_bnd_r": 2.5,
    "BG_RATIO": 1.06,
    "FG_RATIO": 1.02,
    "RADIAL_BG_RATIO": 1.06,
    "RADIAL_FG_RATIO": 1.03,
    "box_list": [
        {
            "t0": 0.9084123335681324,
            "t1": 1.4803756547036238,
            "p0": 0.5719633211354918,
            "p1": 1.5476654571901531,
            "r0": 1.0,
            "r1": 1.06,
            "ds": 0.03,
            "index": 1,
            "radial_ds": 0.003,
            "radial_var_ds": 1.02,
            "tp_var_ds": 1.03
        },
        {
            "t0": 0.9168235588789485,
            "t1": 1.5897215837442324,
            "p0": 4.525239217219033,
            "p1": 5.913091393503681,
            "r0": 1.07,
            "r1": 2.1,
            "ds": 0.01,
            "index": 2,
            "radial_ds": 0.01,
            "radial_var_ds": null,
            "tp_var_ds": 1.02
        },
        {
            "t0": 1.6654226115415764,
            "t1": 2.262619608609516,
            "p0": 2.4813114666907343,
            "p1": 3.2719666459074426,
            "r0": 2.05,
            "r1": 2.4,
            "ds": 0.001,
            "index": 3,
            "radial_ds": 0.015,
            "radial_var_ds": null,
            "tp_var_ds": 1.03
        }
    ]
}
```
  