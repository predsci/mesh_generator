# Example 3D Mesh

Similar to the example of building a [1D Mesh](example1d.md). We can build a 3D Mesh. 

In this example, the 3D Mesh is in polar coordinates ($\theta, \phi, r$). 
## Option #1
First, build the 3 meshes:

```
import numpy 
from mesh_generator.src.mesh import Mesh
from mesh_generator.src.mesh_segment import MeshSegment
from mesh_generator.bin.call_psi_mesh_tool import create_psi_mesh
from mesh_generator.bin.mesh_header_template import write_mesh_header
```
=== "Theta"  
``` python
def theta_test_1():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    MeshSegment.DEFAULT_FG_REGION_RATIO = 1.02
    mesh = Mesh(0.00, numpy.pi, False)
    mesh.insert_mesh_segment(MeshSegment(1.10, 1.40, 0.01))
    mesh.insert_mesh_segment(MeshSegment(1.30, 1.90, 0.02))
    mesh.insert_mesh_segment(MeshSegment(0.40, 2.80, 0.04))
    if mesh.is_valid():
        input_mesh = mesh.json_dict()
        adj_mesh = mesh.resolve_mesh_segments().json_dict()
        legacy_mesh = mesh.build_legacy_mesh().json_dict()
        create_psi_mesh(adj_mesh, legacy_mesh, mesh_type="t", dir_name="../mesh_generator/bin/",
            output_file_name="tmp_mesh_t.dat", mesh_res_file_name="mesh_res_t.dat",
            save_plot=False, show_plot=True, input_mesh=input_mesh)
``` 

=== "Phi"
``` python
def phi_test_1():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
    mesh = Mesh(0.00, 2 * numpy.pi, True)
    mesh.insert_mesh_segment(MeshSegment(3.0, 3.3, 0.01))
    mesh.insert_mesh_segment(MeshSegment(2.8, 3.8, 0.02))
    mesh.insert_mesh_segment(MeshSegment(0.4, 2 * numpy.pi - 0.3, 0.04))
    if mesh.is_valid():
        input_mesh = mesh.json_dict()
        adj_mesh = mesh.resolve_mesh_segments().json_dict()
        legacy_mesh = mesh.build_legacy_mesh().json_dict()
        create_psi_mesh(adj_mesh, legacy_mesh, mesh_type="p", dir_name="../mesh_generator/bin/",
             output_file_name="tmp_mesh_p.dat", mesh_res_file_name="mesh_res_p.dat",
             save_plot=False, show_plot=True, input_mesh=input_mesh)
```

=== "Radial"
``` python
def radial_test_1():
    MeshSegment.DEFAULT_BG_REGION_RATIO = 1.03
    mesh = Mesh(1., 2.5, False)
    mesh.insert_mesh_segment(MeshSegment(1, 1.009, 0.0003, var_ds_ratio=1.02))
    mesh.insert_mesh_segment(MeshSegment(1.0090, 1.009800, 0.00050))
    mesh.insert_mesh_segment(MeshSegment(1.009800, 1.017200, 0.00075))
    mesh.insert_mesh_segment(MeshSegment(1.017200, 1.1000, 0.002))
    mesh.insert_mesh_segment(MeshSegment(1.1000, 1.4000, 0.005))
    mesh.insert_mesh_segment(MeshSegment(1.4000, 2, 0.02))
    if mesh.is_valid():
        input_mesh = mesh.json_dict()
        adj_mesh = mesh.resolve_mesh_segments().json_dict()
        create_psi_mesh(adj_mesh, legacy_mesh, mesh_type="r", dir_name="../mesh_generator/bin/",
            output_file_name="tmp_mesh_r.dat", mesh_res_file_name="mesh_res_r.dat",
            save_plot=False, show_plot=True, input_mesh=input_mesh)
```

Calculate the three meshes above:
``` python
theta_test_1()
phi_test_1()
radial_test_1()
```



Then call `mesh_generator/bin/mesh_header_template.py` to write `mesh_generator/bin/mesh_header.txt` file for MAS. 
``` python
write_mesh_header(path_output="../mesh_generator/bin/", path_header="../mesh_generator/bin/", r_mesh=True)
``` 

## Option #2
Save the mesh requirements in a json/txt file and then call `ui/auto_mesh_from_dict.py`.

For example: `ui_session_files/test_2020_06_04_OP1_phi_advance_mesh.json`:

``` python
{
    "lower_bnd_t": 0.0,
    "upper_bnd_t": 3.141592653589793,
    "lower_bnd_p": 0.0,
    "upper_bnd_p": 6.283185307179586,
    "lower_bnd_r": 0.0,
    "upper_bnd_r": 2.5,
    "BG_RATIO": 1.06,
    "FG_RATIO": 1.06,
    "RADIAL_BG_RATIO": 1.06,
    "RADIAL_FG_RATIO": 1.03,
    "box_list": [
        {
            "t0": 0.2,
            "t1": 2.9,
            "p0": 0.0,
            "p1": 6.2831853,
            "r0": 1,
            "r1": 1.09,
            "ds": 0.02,
            "index": 1,
            "radial_ds": 0.0001,
            "radial_var_ds": null,
            "tp_var_ds": null
        },
        {
            "t0": 0.9252347841897648,
            "t1": 1.724301188717289,
            "p0": 1.4803756547036246,
            "p1": 2.111217553014828,
            "r0": null,
            "r1": null,
            "ds": 0.01,
            "index": 2,
            "radial_ds": null,
            "radial_var_ds": null,
            "tp_var_ds": null
        },
        {
            "t0": 0.9757021360546608,
            "t1": 1.5476654571901518,
            "p0": 4.693463723435355,
            "p1": 4.9962678346247325,
            "r0": null,
            "r1": null,
            "ds": 0.01,
            "index": 3,
            "radial_ds": null,
            "radial_var_ds": null,
            "tp_var_ds": null
        },
        {
            "t0": 0.8747674323248681,
            "t1": 1.6654226115415764,
            "p0": 3.0532747878262247,
            "p1": 3.7850513898672213,
            "r0": null,
            "r1": null,
            "ds": 0.001,
            "index": 4,
            "radial_ds": null,
            "radial_var_ds": null,
            "tp_var_ds": null
        },
        {
            "t0": 1.0934592904060856,
            "t1": 1.2785062472440387,
            "p0": 1.7411236393389222,
            "p1": 2.0102828492850358,
            "r0": null,
            "r1": null,
            "ds": 0.009,
            "index": 5,
            "radial_ds": null,
            "radial_var_ds": null,
            "tp_var_ds": null
        }
    ]
}
```
Then from `ui/03_real_world_example.py` run:

```python
from mesh_generator.ui.auto_mesh_from_dict import MeshRes
import json
import numpy as np

SkipInteratcive = True
UseExistingInput = True

...

if SkipInteractive is True and UseExistingInput is True:
    # Skip Interactive and generate the mesh automatically
    with open("../ui/test_2020_06_04_OP1_phi_advance_mesh.json", 'r') as data:
        ui_dict = json.load(data)
        MeshRes(ui_dict)
```

The resulting `bin/mesh_header.dat` file:
```
&topology 
 nprocs=<NPROC_R>, <NPROC_T>, <NPROC_P> 
 nr=1132
 nt=977
 np=1155
/
&data
 r0=0.0
 r1=2.0
 rfrac=      0.0, 0.4, 0.43600000000000005, 1.0
 drratio=      0.001645715732515603, 1.0, 861.9466194733866
 nfrmesh=5
 tfrac=      0.0, 0.06366197723675814, 0.17084748534747543, 0.2784471218206156,
    0.5301204819277107, 0.5808462620197722, 0.6361402694276503,
    0.9230986699329929, 1.0
 dtratio=      0.6274123713418263, 1.0, 0.05, 1.0, 10.0, 2.0, 1.0,
    1.7908476965428546
 nftmesh=5
 pfrac=      0.0, 0.027313870847428468, 0.0657748065103177, 0.06881320150338566,
    0.11165122024501886, 0.11468961523808682, 0.12771547727313126,
    0.15536248097707034, 0.2026820100709893, 0.2238487246322773,
    0.2776485428688474, 0.39411440632266276, 0.4479142245592328,
    0.4664035629491688, 0.5110457158717358, 0.5386927195756749,
    0.5868854906600123, 0.6144389363158094
 dpratio=      0.4999999999999995, 1.0, 0.8999999999999999, 1.0, 1.1111111111111112,
    1.0, 2.0, 1.0, 1.0, 0.05, 1.0, 20.0, 1.0, 1.0, 0.5, 1.0,
    2.000000000000001, 1.0
 nfpmesh=5
 phishift=1.3087575427128613 
```

## Option #3
**Mesh UI (User Interface)**

The tkinter [user interface](ui.md) is an easy way to create 3D meshes by visualizing a magnetogram. 
```python
from mesh_generator import MeshGeneratorUI
MeshGeneratorUI()
```


