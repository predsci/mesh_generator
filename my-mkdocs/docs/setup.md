
Follow these steps to install the "[mesh_generator](https://pypi.org/project/mesh-generator/)" package containing bin and src modules.
More information about how to [package Python projects](https://packaging.python.org/tutorials/packaging-projects/).

**1)** Check python version is >= 3.5.6 with the following bash command.

```bash
$ python --version
Python 3.7.6
```


**2)** Use the package manager [pip](https://pip.pypa.io/en/stable/) to install mesh_generator.
Recommended to install the package in a [virtual environment](https://docs.python.org/3/tutorial/venv.html).

```bash
$ pip install mesh-generator
```


**4)** Validate installation.

```bash
$ python
Python 3.7.6 (default, Jan  8 2020, 13:42:34)
[Clang 4.0.1 (tags/RELEASE_401/final)] :: Anaconda, Inc. on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import mesh_generator
>>> help(mesh_generator)
```

**5)** If you want to download the full [repo](https://bitbucket.org/predsci/mesh_generator/src/master/) 
which includes the [unittests](unittest.md). Then: 
```bash
$ git clone <repo> 
```