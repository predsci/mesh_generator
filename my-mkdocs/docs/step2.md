

**Step 2** of creating a MAS mesh will adjust the mesh segments by calling the function `resolve_mesh_segements()` in 
`mesh_generator/src/mesh.py`. This function will call the following functions (in ascending order):
```python
def resolve_mesh_segments(self):
    """
    Step 2 of building a mesh. Returns the adjusted mesh.
    """
    self._resolve_mesh_segments_overlap_region()  
    self._resolve_mesh_segments_narrow_region() 
    self._resolve_mesh_segments_ds()  
    if self.periodic: 
        self._periodic_case()

    return self
```

- 1) `_resolve_mesh_segments_overlap_region()`:

    Check if there is mesh segments are overlapping. If so, it will always keep the segment with the
    lower resolution.

- 2) `_resolve_mesh_segments_narrow_region()`:

    Remove all small mesh segments that are 4 or less points, and instead tag the small segment domain to the
    segment adjacent to it that has the smaller ds.


- 3) `_resolve_mesh_segments_ds()`:

    Make the current mesh continuous. Set a $ds_{0}$ and $ds_{1}$ for each segment.
    Meaning, $ds_{0}$ is the beginning resolution and $ds_{1}$ is the resolution at the end of the segment.


- 4) `_periodic_case()`

    If mesh is periodic `_periodic_case` will adjust the begin and last segments so the adjusted mesh
    will be periodic.
    
    
=== "Periodic case"

    Here is how the  `_periodic_case ` function which is part of **step 2** of creating a mesh adjusts the begin and end segments
    according to the [mesh types](equations.md). 
    
    For the case of additional calculations we shift the mesh which adds a  `phi_shift `to the [legacy mesh](step3.md). 
    
    ![Screenshot](images/periodic_case_flow.png)


=== "Resolve gap"
    ![Screenshot](images/resolve_gap.png)


## Example  
Mesh input `test/ar_test.py` theta test #1: 
```python
MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
mesh = Mesh(0.00, numpy.pi, False)
mesh.insert_mesh_segment(MeshSegment(1.10, 1.40, 0.01))
mesh.insert_mesh_segment(MeshSegment(1.30, 1.90, 0.02))
mesh.insert_mesh_segment(MeshSegment(0.40, 2.80, 0.04))
```
![Screenshot](images/step2plot.png)

