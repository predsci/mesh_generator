**step 3** of building a mesh. Main function: `build legacy mesh()` in `mesh_generator/src/mesh.py`. Here we take the 
[adjusted_mesh](step2.md) segments 
and calculate each segment's:

 - Total number of points.
 - Domain (length).
 - Cell-to-cell ratio. 
 
The calculations to solve legacy mesh segments are in `src/mesh_segments`.
`build_legacy_mesh() ` adds a layer of complexity since it solves the legacy mesh iteratively.
Each time a mesh segment is *undershooting* the legacy mesh is recalculated.

## Example
For example: theta test #1 in `test/unit_test.py`:

**Requirements (mesh input)**:
    
```python
MeshSegment.DEFAULT_BG_REGION_RATIO = 1.06
mesh = Mesh(0.00, numpy.pi, False)
mesh.insert_mesh_segment(MeshSegment(1.10, 1.40, 0.01))
mesh.insert_mesh_segment(MeshSegment(1.30, 1.90, 0.02))
mesh.insert_mesh_segment(MeshSegment(0.40, 2.80, 0.04))
return mesh
```

**Adjusted mesh results**:
```python
{
  "lower_bnd": 0.0,
  "upper_bnd": 3.141592653589793,
  "periodic": false,
  "phi_shift": 0.0,
  "BG_RATIO": 1.06,
  "segment_list": [
    {
      "s0": 0.0,
      "s1": 0.4,
      "ds0": Infinity,
      "ds1": 0.04,
      "var_ds_ratio": 1.06,
      "ratio": 0.9433962264150942
    },
    {
      "s0": 0.4,
      "s1": 1.1,
      "ds0": 0.04,
      "ds1": 0.01,
      "var_ds_ratio": 1.03,
      "ratio": 0.970873786407767
    },
    {
      "s0": 1.1,
      "s1": 1.4,
      "ds0": 0.01,
      "ds1": 0.01,
      "var_ds_ratio": 1.03,
      "ratio": 1.0
    },
    {
      "s0": 1.4,
      "s1": 1.9,
      "ds0": 0.01,
      "ds1": 0.02,
      "var_ds_ratio": 1.03,
      "ratio": 1.03
    },
    {
      "s0": 1.9,
      "s1": 2.8,
      "ds0": 0.02,
      "ds1": 0.04,
      "var_ds_ratio": 1.03,
      "ratio": 1.03
    },
    {
      "s0": 2.8,
      "s1": 3.141592653589793,
      "ds0": 0.04,
      "ds1": Infinity,
      "var_ds_ratio": 1.06,
      "ratio": 1.06
    }
  ]
}
```

**Legacy mesh results**:
```python
{
  "lower_bnd": 0.0,
  "upper_bnd": 3.141592653589793,
  "periodic": false,
  "phi_shift": 0.0,
  "BG_RATIO": 1.06,
  "segment_list": [
    {
      "s0": 0.0,
      "s1": 0.4,
      "ratio": 0.5583947769151176,
      "num_points": 10
    },
    {
      "s0": 0.4,
      "s1": 1.1,
      "ratio": 0.32522615237693164,
      "num_points": 38
    },
    {
      "s0": 1.1,
      "s1": 1.4,
      "ratio": 1.0,
      "num_points": 30
    },
    {
      "s0": 1.4,
      "s1": 1.749403232925598,
      "ratio": 2.0,
      "num_points": 24
    },
    {
      "s0": 1.749403232925598,
      "s1": 1.9,
      "ratio": 1.0,
      "num_points": 8
    },
    {
      "s0": 1.9,
      "s1": 2.5988064658511965,
      "ratio": 2.0,
      "num_points": 24
    },
    {
      "s0": 2.5988064658511965,
      "s1": 2.8,
      "ratio": 1.0,
      "num_points": 6
    },
    {
      "s0": 2.8,
      "s1": 3.141592653589793,
      "ratio": 1.5036302589913606,
      "num_points": 7
    }
  ]
}
```
** plot of legacy mesh points. Results are found in `bin/mesh_res.txt`**
![](images/theta_mesh_test_1.png)