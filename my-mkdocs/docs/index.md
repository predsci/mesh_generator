# Mesh Generator

This is a collection of Python subroutines and examples that illustrate how to build a
MAS mesh.


## Workflow
1) **Step 1**: Input mesh requirements.

2) **Step 2**: `mesh_generator/src/mesh.py` function `resolve_mesh_segments()` - Build [adjusted mesh](step2.md). 

3) **Step 3**: `mesh_generator/src/mesh.py` function `build_legacy_mesh()` - Build [legacy mesh](step3.md). 

4) `bin/create_psi_mesh.py`- Write a file with mesh points, cell resolution and ratio. 
Including optimizing the mesh total number of points.

5) `bin/tmp_mesh_[t/p/r].dat` `bin/mesh_header.dat` `bin/mesh_res_[t/p/r].txt` - output. 
![Screenshot](images/mesh_generator_flowchart.png)


## Dependencies

- The primary dependencies are the PSI tools (Mesh) and Python >= 3.7.0.
- Python library dependencies:
```python
h5py>=2.8.0 
pyhdf>=0.9.10
numpy>=1.15.2
tk>=8.6.0 
matplotlib>=3.0.0
python>=3.5.0
scipy>=1.1.0 
```

## Important Python subroutines
The most important Python subroutines for mesh generation are:

  - src: (step 1,2,3)
  
        mesh_generator/src/mesh.py
        mesh_generator/src/mesh_segment.py
        mesh_generator/src/legacy_mesh_segment.py
        mesh_generator/src/legacy_mesh.py
        mesh_generator/src/mesh_optimizer.py

  - bin: (step 4,5)
  
        mesh_generator/bin/call_psi_mesh.py
        mesh_generator/bin/plot_legacy_mesh.py
        
        

