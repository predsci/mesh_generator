
# Solution Details

The mesh is built from an array of mesh segments that have positions ($s_{0}$, $s_{1}$), and
resolution constraint ($ds$).

The legacy mesh needs to determine the number of points $n$ for each segment
from $s_{0}$ to $s_{1}$, satisfying certain resolution constrains.
$R$ is the cell-to-cell ratio.
$$
ds(n) = ds_{0} \cdot R^{n}
$$
Calculating mesh segments is divided into **three** cases:

### **1) CONSTANT** 
simplest case.
$$
R=1 
$$
$$
ds_{0} = ds_{1}
$$

$$
n = \frac{s_{1}-s_{0}}{ds_{0}}
$$


### **2) FIXED LENGTH**
Go a fixed distance at a specified ratio starting at a given
$ds_{0}$. 
Usually begin/end segments, segment gaps, or fixed ds undershooting case. 
Integrate the $ds(n)$ equation:
$$
S = \frac{ds_{0} \cdot (R^{n} - 1)}{ln(R)}
$$
Solve this for n:
$$
 n = \frac{ln[ \frac{ln(R) \cdot S}{ds_{0}} +1]}{ln(R)}
$$

### **3) FIXED DS** 
Specify the ds at both sides ($ds_{0}$, and $ds_{1}$) and attempt to find a
solution that does this in the fewest number of points, at a fixed $R$.

Solve:

$$
ds_{1} = ds_{0} \cdot R^{n}
$$

which is 
$$
n = \frac{ln(\frac{ds_{1}}{ds_{0}})}{ln(R)}
$$

The length $S$ is: 
$$
S = \frac{ds_{0} \cdot (R^{n} - 1)}{ln(R)}
$$

- **HOWEVER**, the length is not constrained to be $S = s_{1} - s_{0}$. $S$ can be shorter or longer. 

    **1) shorter case**: mesh segment overhsooting case. Simple, add **constant mesh** at larger ds to make up the gap.
    
    **2) longer case **: mesh segment undershooting case. More complex, means you have to go back to previous or next 
    mesh segment and adjust its length and resolution ($S$ and $ds_{0}$ or $ds_{1}$). 
    
    **NOTE**: A solution may not be found if it crosses others.
    
     - Solving this issue adds most of the code complexity because of the legacy mesh recursion 
     and a lot of case checking.
