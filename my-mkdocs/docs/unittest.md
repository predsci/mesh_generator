## The unit tests are found in `tests/unit_test.py`.

The tests compare with the dictionary results in in `tests/test_results.py`, 
checking both **step #2** ([adjust mesh segments](step2.md)) and **step #3** ([legacy mesh](step3.md)) of the mesh calculations.
Then will compare with the `output.dat` file saved in the folder named `output` and test **step #4**
(writing output files and calculating optimal total number of points in mesh spacing).

- Last update: October 19th, 2020.

- Current tests:  17 Theta, 38 Phi, 5 Radial. (total of 60).

- Note: 
      - All phi cases are periodic.
      - Best way to create a test is as the tests found in `tests/unit_test.py` or in the format used in `tests/ar_test.py`
      (using 3 different functions). It is important you save the step 2 (adjusted mesh) as 
      a dictionary before computing the legacy mesh. This  dictionary is needed for `check_mesh_valid()` which 
      will check iteratively if the mesh is above user requests.
      - An example test can also be found in `tests/create_mesh.py`.


## Run unittests from terminal:


**1)** Check python version is >= 3.5.6 with the following bash command.

```bash
$ python --version
Python 3.7.6
```

**2)** Create a new conda environment. 

```bash
$ conda create --name meshtest python=3.7
```

**3)** Activate the new conda environment. 
 
```bash
$ conda activate meshtest
```

**4)** Use the package manager [pip](https://pip.pypa.io/en/stable/) to install mesh_generator.
```bash
$ pip install mesh-generator
```

**5)** Validate installation.

```bash
$ python
Python 3.7.6 (default, Jan  8 2020, 13:42:34)
[Clang 4.0.1 (tags/RELEASE_401/final)] :: Anaconda, Inc. on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import mesh_generator
>>> help(mesh_generator)
```


**5)** Download Bitbucket [repository](https://bitbucket.org/predsci/mesh_generator/src/master/). 
The unittests are in the module "unit_test.py" in "tests" folder. 
```bash
$ git clone <repo> 
```      

**6)** Run unit tests from command line. 
```bash
$ conda activate meshtest
$ cd path-to-tests-folder # example: pycharmprojects/mesh_generation/tests 
$ python -m unittest unit_test.py
............................................................
----------------------------------------------------------------------
Ran 60 tests in 8.800s

OK
```      
